//
//  ViewController.swift
//  Ticket-GO
//
//  Created by mac on 29/03/22.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var txtBaseUrl: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        if IS_DEMO_MODE == true {
            self.txtBaseUrl.isUserInteractionEnabled = false
        }
    }
    
    
    @IBAction func onClick_login(_ sender: Any)
    
    {
        if txtBaseUrl.text! == ""{
            self.view.makeToast("Please provide Base URL for App")
        }else{
            let urlString = txtBaseUrl.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//            if urlString?.last != "/"{
//                printD("sssssss")
//            }
            URL_BASE = urlString!
            setValueToUserDefaults(value: urlString as AnyObject, key: userDefaultsKeys.KEY_BASE_URL_APP)
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoginEmailViewController") as! LoginEmailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onClick_signup(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }


}

