//
//  CustomFieldCell.swift
//  Ticket-GO
//
//  Created by mac on 29/07/22.
//

import UIKit

class CustomFieldCell: UITableViewCell {
    @IBOutlet weak var lblCustomField: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ customfield: customFieldModel){
        self.lblCustomField.text = customfield.name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
