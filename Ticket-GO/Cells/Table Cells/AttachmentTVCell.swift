//
//  AttachmentTVCell.swift
//  Ticket-GO
//
//  Created by mac on 27/07/22.
//

import UIKit

class AttachmentTVCell: UITableViewCell {
    @IBOutlet weak var lblAttachmentName: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var btnDelete: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
