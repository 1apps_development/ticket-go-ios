//
//  FaQCell.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit

class FaQCell: UITableViewCell {
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellWith(_ faq: faqModel)
    {
        lblId.text = "\(faq.id!)"
        lblTitle.text = faq.title
        lblDesc.text = faq.descreption
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
