//
//  TicketFieldCell.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit

class TicketFieldCell: UITableViewCell {
    @IBOutlet weak var txtLabels: UITextField!
    @IBOutlet weak var txtPlaceholder: UITextField!
    @IBOutlet weak var btnDelete: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ dict:[String:Any]){
        txtLabels.text = dict["name"] as? String
        txtPlaceholder.text = dict["placeholder"] as? String
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
