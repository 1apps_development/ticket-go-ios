//
//  TicketsListCell.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit

class TicketsListCell: UITableViewCell {
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTicketNo: UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblCreatedat: UILabel!
    @IBOutlet weak var lblSubjectTitle: UILabel!
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var viewStatusBG: UIView!
    @IBOutlet weak var view_categories: ShadowView!
    
    let InprogressColor = "FFC700"
    let ClosedColor = "6FD943"
    let OnHoldColor = "FC275A"
    let ClosedBGColor = "F6FDF3"
    let OnHoldBGColor = "FFF6F9"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ ticket: TicketsModel){
        lblCategory.text = ticket.category
        lblStatus.text = ticket.status
        lblName.text = ticket.name
        lblTicketNo.text = "#\(ticket.ticketId!)"
        lblEmail.text = ticket.email
        lblSubjectTitle.text = ticket.subject
        lblCreatedat.text = ticket.times
        //        viewOuter.layer.borderColor = hexStringToUIColor(hex: ticket.color!).cgColor
        if ticket.category == "" {
            self.view_categories.isHidden = true
        } else {
            self.view_categories.isHidden = false
        }
        if ticket.status == "In Progress"{
            viewOuter.backgroundColor = .white
            viewOuter.layer.borderColor = hexStringToUIColor(hex: "DCE2ED").cgColor
            viewStatusBG.backgroundColor = hexStringToUIColor(hex: InprogressColor).withAlphaComponent(0.2)
            lblStatus.textColor = hexStringToUIColor(hex: InprogressColor)
            imgStatus.image = UIImage(named: "ic_inprog")
        }else if ticket.status == "On Hold"{
            viewOuter.backgroundColor = hexStringToUIColor(hex: OnHoldBGColor)
            viewOuter.layer.borderColor = hexStringToUIColor(hex: OnHoldColor).cgColor
            viewStatusBG.backgroundColor = hexStringToUIColor(hex: OnHoldColor).withAlphaComponent(0.2)
            lblStatus.textColor = hexStringToUIColor(hex: OnHoldColor)
            imgStatus.image = UIImage(named: "close_bold")
            
        }else if ticket.status == "Closed"{
            viewOuter.backgroundColor = hexStringToUIColor(hex: ClosedBGColor)
            viewOuter.layer.borderColor = hexStringToUIColor(hex: ClosedColor).cgColor
            viewStatusBG.backgroundColor = hexStringToUIColor(hex: ClosedColor).withAlphaComponent(0.2)
            lblStatus.textColor = hexStringToUIColor(hex: ClosedColor)
            imgStatus.image = UIImage(named: "ic_closed")
            
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
