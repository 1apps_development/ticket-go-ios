//
//  TicketMoreCell.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit

class TicketMoreCell: UITableViewCell {
    @IBOutlet weak var lblTicketNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewBox: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(_ ticket: TicketsModel){
        self.lblTicketNo.text = "#\(ticket.ticketId!)"
        self.lblName.text = ticket.name
        self.lblEmail.text = ticket.email
        //self.viewBox.layer.borderColor = hexStringToUIColor(hex: ticket.color!).cgColor
    }
}
