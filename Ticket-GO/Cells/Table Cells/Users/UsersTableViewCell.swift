//
//  UsersTableViewCell.swift
//  Taskly
//
//  Created by mac on 14/03/22.
//

import UIKit
import MultiProgressView
import SDWebImage

class UsersTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblEmail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }
    func configureCell(_ user: userModel){
        lblUsername.text = user.name
        lblEmail.text = user.email
        if let imgUrl = URL(string: user.avtar!){
            imgUserProfile.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UsersTableViewCell: MultiProgressViewDelegate, MultiProgressViewDataSource{
    
    
    func numberOfSections(in progressView: MultiProgressView) -> Int{
        return 3
    }
    func progressView(_ progressView: MultiProgressView, viewForSection section: Int) -> ProgressViewSection{
        let progressSection = ProgressViewSection()
        if section == 0{
            progressSection.backgroundColor = hexStringToUIColor(hex: "6FD943")
        }else if section == 1{
            progressSection.backgroundColor = hexStringToUIColor(hex: "153364")
        }else if section == 2{
            progressSection.backgroundColor = hexStringToUIColor(hex: "F0F1F5")
        }
        return progressSection
    }

}
