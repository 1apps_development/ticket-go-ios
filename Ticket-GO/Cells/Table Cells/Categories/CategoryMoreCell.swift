//
//  CategoryMoreCell.swift
//  Ticket-GO
//
//  Created by mac on 08/08/22.
//

import UIKit

class CategoryMoreCell: UITableViewCell {
    @IBOutlet weak var viewDot: UIView!
    @IBOutlet weak var lblCategoryname: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ category: categoryMoreModel){
        self.viewDot.backgroundColor = hexStringToUIColor(hex: category.color!)
        lblCategoryname.text = category.category
        lblCategoryname.textColor = hexStringToUIColor(hex: category.color!)
        lblPercentage.text = String(format: "%.2d %%", category.value!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
