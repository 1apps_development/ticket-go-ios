
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
//        
//        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_IS_USER_LOGGED_IN) != nil{
//            guard let _ = (scene as? UIWindowScene) else { return }
//            let window = UIWindow(windowScene: scene as! UIWindowScene)
//            self.window = window
//            self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
//            self.window?.makeKeyAndVisible()
//        }
    }
    
    public func logoutWithoutAPI(_ isLogout: Bool = false){
        if isLogout{
        resetDefaults()
        }
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
}

