//
//  EditProfileVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit
import Alamofire


class EditProfileVC: UIViewController {
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCnfPassword: UITextField!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblImgname: UILabel!
    
    var userId: Int = 0
    var avtarStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setProfileData()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Function
    
    func setProfileData()
    {
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USER_REG_DATA) != nil{
            if let data = readValueFromUserDefaults(userDefaultsKeys.KEY_USER_REG_DATA) as? [String:Any]
            {
                if let imgStr = data["image_url"] as? String{
                    if let imgUrl = URL(string: imgStr){
                        imgUser.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
                        self.avtarStr = "\(imgUrl)"
                    }
                }
                let email = data["email"] as! String
                self.userId = data["id"] as! Int
                self.txtFullName.text = data["name"] as? String
                self.txtEmail.text = email
                if data["avatar"] as? String == nil {
                    self.lblImgname.text! = self.avtarStr
                }
                else {
                    self.lblImgname.text = data["avatar"] as? String
                }
            }
        }
    }
    
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickUpdateProfile(_ sender: Any)
    {
        var msg:String = ""
        
        if txtFullName.text! == ""{
            msg = "FullName Field Should Not Empty.!"
        }else if txtEmail.text! == ""{
            msg = "Email Field Should Not Empty.!"
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            msg = "Please enter valid Email Address"
        }else if txtPassword.text! == ""{
            msg = "Password Field Should Not Empty.!"
        }else if txtCnfPassword.text! == ""{
            msg = "Confirm Password Field Should Not Empty.!"
        }else if txtPassword.text! != txtCnfPassword.text{
            msg = "Password and Confirm Password not Matched.!"
        }else if lblImgname.text! == ""{
            msg = "Please add an Attachment.!"
        }else{
            if IS_DEMO_MODE == true {
                self.view.makeToast("You can't access this functionality as a demo user")
            } else {
                var param:[String:String] = [:]
                param = ["id":"\(self.userId)",
                         "name":txtFullName.text!,
                         "email":txtEmail.text!,
                         "password":txtPassword.text!,
                         "password_confirmation":txtCnfPassword.text!]
                
                self.MakeAPICallforUpdateProfile(param)
            }
            
        }
        self.view.makeToast(msg)
    }
    
    @IBAction func onClickImage(_ sender: Any)
    {
        ImagePickerManager().pickImage(self){ image in
            self.imgUser.image = image
            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let attachmet = AttachmentItem(imgname: "\(randomString(length: 5)).jpg", imgsize: Double(imageSize), img: image)
            self.lblImgname.text = "img_\(randomString(length: 5)).png"
            print("actual size of image in KB: %f ", Double(imageSize) / 10000.0)
            //here is the image
        }
    }
    
    
    //MARK: - APIs
    
    func MakeAPICallforUpdateProfile(_ dict:[String:String]){
        SHOW_CUSTOM_LOADER()
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in dict{
                multipartFormData.append((value).data(using: .utf8)!, withName: key)
            }
            if let jpegData = self.imgUser.image!.jpegData(compressionQuality: 0.4) {
                multipartFormData.append(jpegData, withName: "avatar", fileName: "image_\(randomString(length: 4)).jpg", mimeType: "image/jpeg")
            }
        },
                  to: URL_CreateUser, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                if response.response?.statusCode == 200 {
                    print("OK. Done")
                    do {
                        
                        if let jsonDict = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? NSDictionary
                        {
                            print(jsonDict) // use the json here
                            if let _ = jsonDict["message"] as? String{
                                //                                if let token = data["access_token"] as? String{
                                //                                    setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                //                                }
                                var dict:[String:Any] = [:]
                                if let data = jsonDict["data"] as? [String: Any]{
                                    if let user = data["user"] as? [String: Any]{
                                        
                                        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USER_REG_DATA) != nil{
                                            if let udfData = readValueFromUserDefaults(userDefaultsKeys.KEY_USER_REG_DATA) as? [String:Any]{
                                                dict = udfData
                                                dict["name"] = user["name"]
                                                dict["email"] = user["email"]
                                                dict["image_url"] = user["avatarlink"]
                                                setValueToUserDefaults(value: dict as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                            }
                                        }
                                    }
                                }
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                //                                self.view.makeToast(msg, point: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2), title: "User Created Successfully!", image: nil) { didTap in
                                //                                }
                            }
                            //                            if let data = jsonArray["data"] as? [String:Any]{
                            //
                            //                            }
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }else if response.response?.statusCode == 406{
                    print("error")
                    DispatchQueue.main.sync{
                        let alert = UIAlertController(title: "Error", message:
                                                        "Person has not been set up.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                            DispatchQueue.main.async{
                                HIDE_CUSTOM_LOADER()
                                
                                //                                self.progressUiView.isHidden = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }))
                        
                        self.present(alert, animated: true)
                    }
                }else{
                    print(response.response?.statusCode)
                }
                
                
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        })
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
