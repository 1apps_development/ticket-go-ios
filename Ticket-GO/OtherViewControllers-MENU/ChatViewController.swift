//
//  ChatViewController.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class ChatViewController: UIViewController {
    @IBOutlet weak var peopleCV: UICollectionView!
    @IBOutlet weak var tablePeopleChat: UITableView!
    @IBOutlet weak var lbl_date_corner: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_date_corner.text = getCurrentDate()

        peopleCV.register(UINib.init(nibName: "ChatBubbleCell", bundle: nil), forCellWithReuseIdentifier: "ChatBubbleCell")
        tablePeopleChat.register(UINib.init(nibName: "ChatListCell", bundle: nil), forCellReuseIdentifier: "ChatListCell")
        tablePeopleChat.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

}

extension ChatViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatBubbleCell", for: indexPath) as! ChatBubbleCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }

}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as! ChatListCell
        cell.selectionStyle = .none
        return cell
    }
}
