//
//  CreateFaqVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit
import Alamofire

class CreateFaqVC: UIViewController {
    @IBOutlet weak var lblCreateBtn: UILabel!
    @IBOutlet weak var lblNewfaq: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var txtTitle: UITextField!
    
    var isUpdating:Bool = false
    var faqData: faqModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Function
    
    func setupInitialUI(){
        
        if isUpdating{
            self.setUpdatableData()
        }
    }
    
    func setUpdatableData(){
        self.lblNewfaq.text = "Edit FAQ"
        self.lblCreateBtn.text = "Update"
        self.txtTitle.text = faqData.title
        self.txtDesc.text = faqData.descreption
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any){
        var msg: String = ""
        if txtTitle.text! == ""{
            msg = "Title Field Should Not Empty.!"
        }else if txtDesc.text! == "" {
            msg = "Description Field Should Not Empty.!"
        }else{
            var params:[String:Any] = [:]
            if isUpdating{
                params = ["id":faqData.id!,
                          "title":txtTitle.text!,
                          "description":txtDesc.text!]
            }else{
                params = [
                          "title":txtTitle.text!,
                          "description":txtDesc.text!]
            }
            self.MakeAPICallforCreateFAQ(params)
            
        }
        self.view.makeToast(msg)

    }
    
    //MARK: - API
    
    func MakeAPICallforCreateFAQ(_ params: [String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_CreateFAQ, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let _ = json["data"] as? [String:Any]{
                                    
                                    let imageDataDict:[String: Bool] = ["isUpdated": true]
                                    
                                    NotificationCenter.default.post(name: Notification.Name("REFRESH_FAQ"), object: nil, userInfo: imageDataDict)
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
