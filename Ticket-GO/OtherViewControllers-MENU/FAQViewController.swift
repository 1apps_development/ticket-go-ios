//
//  FAQViewController.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit
import Alamofire

class FAQViewController: UIViewController {
    
    @IBOutlet weak var tableFaq: UITableView!
    @IBOutlet weak var lbl_date_corner: UILabel!
    @IBOutlet weak var lbl_NoDAtaFound: UILabel!
    
    var ArrFAQs: [faqModel] = []
    var nextPageUrl: String = ""
    var lastPageUrl: String = ""
    var lastPage: Int = 0
    var totalData: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_date_corner.text = getCurrentDate()
        self.lbl_NoDAtaFound.isHidden = false
        tableFaq.register(UINib(nibName: "FaQCell", bundle: nil), forCellReuseIdentifier: "FaQCell")
        tableFaq.tableFooterView = UIView()
        
        let param:[String:Any] = ["id":getID()]
        self.MakeAPICallforFAQList(param)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name("REFRESH_FAQ"), object: nil)
    }
    
    //MARK: - Other Functions
    
    @objc func editFaq(_ sender: UIButton){
        let vc = CreateFaqVC(nibName: "CreateFaqVC", bundle: nil)
        vc.faqData = ArrFAQs[sender.tag]
        vc.isUpdating = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func DeleteFaq(_ sender: UIButton){
        
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        } else {
            self.MakeAPICallforDeleteFAQ(["id":ArrFAQs[sender.tag].id!])
        }
    }
    
    @objc func notificationReceived(_ notification: NSNotification){
        if let isnew = notification.userInfo?["isUpdated"] as? Bool {
            if isnew{
                self.refreshFaqList(isnew)
            }
        }
    }
    
    func loadMoreItemsForList(){
        
        if totalData == ArrFAQs.count{
            //            self.viewShowMore.isHidden = true
        }else{
            //            self.viewShowMore.isHidden = false
            self.refreshFaqList(false)
        }
    }
    
    
    @objc func refreshFaqList(_ isNewFaq:Bool = true){
        if isNewFaq{
            self.ArrFAQs.removeAll()
            self.nextPageUrl = ""
        }
        let param:[String:Any] = ["id":getID()]
        self.MakeAPICallforFAQList(param)
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddNewFaq(_ sender: Any){
        let vc = CreateFaqVC(nibName: "CreateFaqVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - API
    
    func MakeAPICallforFAQList(_ params: [String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(self.nextPageUrl == "" ? URL_FAQList : self.nextPageUrl, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let faq = data["faq"] as? [String:Any]{
                                        self.totalData = faq["total"] as! Int
                                        if let next = faq["next_page_url"] as? String{
                                            self.nextPageUrl = next
                                        }
                                        if let last = faq["last_page_url"] as? String{
                                            self.lastPageUrl = last
                                        }
                                        if let faqar = faq["data"] as? [[String:Any]]{
                                            self.ArrFAQs.append(contentsOf: faqSuperModel.init(faqar).faqs)
                                            self.tableFaq.reloadData()
                                        }
                                        if self.ArrFAQs.count == 0 {
                                            self.lbl_NoDAtaFound.isHidden = false
                                        } else {
                                            self.lbl_NoDAtaFound.isHidden = true
                                        }
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    
    func MakeAPICallforDeleteFAQ(_ params: [String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_DeleteFAQ, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }
                            else if status == 1{
                              //  self.refreshFaqList(false)
//                                if json["data"] is [String:Any]{
//                                    let param:[String:Any] = ["id":getID()]
//                                    self.MakeAPICallforFAQList(param)
//                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension FAQViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ArrFAQs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaQCell", for: indexPath) as! FaQCell
        cell.selectionStyle = .none
        cell.configureCellWith(ArrFAQs[indexPath.row])
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(editFaq(_:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteFaq(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                self.loadMoreItemsForList()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        128
    }
}
