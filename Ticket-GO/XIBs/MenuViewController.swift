//
//  MenuViewController.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import UIKit
import SOTabBar
import Alamofire
import SDWebImage
protocol MenuOptionSelectDelegate: NSObject{
    func menuOptionSelected(_ title: String)
}

class MenuViewController: UIViewController {
    @IBOutlet weak var menuCollection: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    
    var mDelegate: MenuOptionSelectDelegate?
    var parentVC: UIViewController!
    var selectedItem: String!
    var arrMenuItems = [MenuItem(title: "Dashboard", image: "tab_dash_sel", isselected: true),
                        MenuItem(title: "Users", image: "tab_user_sel", isselected: false),
                        MenuItem(title: "Tickets", image: "tab_tickets", isselected: false),
                        MenuItem(title: "Categories", image: "tab_categorie_sel", isselected: false),
                        MenuItem(title: "FAQ", image: "faq", isselected: false),
                        MenuItem(title: "Settings", image: "tab_setting_sel", isselected: false)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        for item in arrMenuItems{
            if selectedItem == item.title{
                item.isSelected = true
            }else{
                item.isSelected = false
            }
        }
        lblTitle.text = selectedItem != nil ? selectedItem : "Dashboard"
        menuCollection.register(UINib.init(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        self.setProfileData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//        view.layer.add(transition, forKey: kCATransition)
        self.view.alpha = 1
    }
    
    //MARK: - Custom Function
    
    func setProfileData()
    {
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USER_REG_DATA) != nil{
            if let data = readValueFromUserDefaults(userDefaultsKeys.KEY_USER_REG_DATA) as? [String:Any]
            {
                if let imgStr = data["image_url"] as? String{
                    if let imgUrl = URL(string: imgStr){
                        imgUserProfile.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
                    }
                }
                let email = data["email"] as! String
                lblUsername.text = data["name"] as? String
                lblEmail.text = email
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
//        let transition:CATransition = CATransition()
//        transition.duration = 0.3
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromTop
//        view.layer.add(transition, forKey: kCATransition)
//        self.view.alpha = 0

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickEditProfile(_ sender: Any){
        let vc = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext

        self.dismiss(animated: false) {
            self.parentVC.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}
extension MenuViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 3.6)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.configureCell(arrMenuItems[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let _ = self.mDelegate{
//            self.mDelegate?.menuOptionSelected(arrMenuItems[indexPath.row].title)
        self.dismiss(animated: false) {
            if let _ = self.parentVC{

                let title = self.arrMenuItems[indexPath.item].title
                var viewContr = UIViewController()
                var isTabVC:Bool = false
                
                if title == "Dashboard"{
                    let imageDataDict:[String: Int] = ["tab": 2]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Users"{
                    let imageDataDict:[String: Int] = ["tab": 3]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Settings"{
//                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SettingsVC")
                    let imageDataDict:[String: Int] = ["tab": 4]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)


                }else if title == "Chat"{
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ChatViewController")
                }else if title == "FAQ"{
                    viewContr = iPhoneStoryBoard.instantiateViewController(withIdentifier: "FAQViewController")
                }else if title == "Categories"{
                    let imageDataDict:[String: Int] = ["tab": 1]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)
                }else if title == "Tickets"{
                    let imageDataDict:[String: Int] = ["tab": 0]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }
                
                
                if !isTabVC{
                    self.parentVC.navigationController?.pushViewController(viewContr, animated: true)
                }

            }

        }
//        }
    }

}
