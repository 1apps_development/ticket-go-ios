//
//  TicketsVC.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit
import Alamofire


class TicketsVC: UIViewController {
    @IBOutlet weak var tableTickets: UITableView!
    @IBOutlet weak var tableMoreTickets: UITableView!
    @IBOutlet weak var lblNewTickets: UILabel!
    @IBOutlet weak var lblClosedTickets: UILabel!
    @IBOutlet weak var lblOpenTickets: UILabel!
    @IBOutlet weak var lblNewTicketsCenter: UILabel!
    @IBOutlet weak var lbl_date_corner: UILabel!
    @IBOutlet weak var NewTicketsCircle: KDCircularProgress!
    @IBOutlet weak var OpenTicketsCircle: KDCircularProgress!
    @IBOutlet weak var ClosedTicketsCircle: KDCircularProgress!
    @IBOutlet weak var height_ticketTableview: NSLayoutConstraint!
    @IBOutlet weak var view_empty: UIView!
    @IBOutlet weak var lbl_noDataFound: UILabel!
    @IBOutlet weak var height_tableviewMore: NSLayoutConstraint!
    
    private var internalData : [WrapperObject] = []
    var arrInprogressTickets:[TicketsModel] = []
    var arrHoldTickets:[TicketsModel] = []
    var arrClosedTickets:[TicketsModel] = []
    
    
    var arrTicketsMore:[TicketsModel] = []
    var analytics = NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_date_corner.text = getCurrentDate()
        let nib = UINib(nibName: "TicketHeader", bundle: nil)
        let cellnib = UINib(nibName: "TicketsListCell", bundle: nil)
        self.view_empty.isHidden = false
        tableTickets.register(nib, forHeaderFooterViewReuseIdentifier: "TicketHeader")
        tableTickets.register(cellnib, forCellReuseIdentifier: "TicketsListCell")
        tableTickets.tableFooterView = UIView()
        tableMoreTickets.register(UINib.init(nibName: "TicketMoreCell", bundle: nil), forCellReuseIdentifier: "TicketMoreCell")
        tableMoreTickets.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterupdate), name: Notification.Name("REFRESH_TICKET"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterupdate), name: Notification.Name("REFRESH_REPLAY_TICKET"), object: nil)
        refreshTickets()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshTickets()
        
    }
    
    @objc func refreshViewAfterupdate(){
        refreshTickets()
    }

    
    //MARK: - Custom Functions
    
    func setDataOfChart(_ dict: NSDictionary){
        let newTicketPer = dict["new_ticket"] as! Int
        let openTicketPer = dict["open_ticket"] as! Int
        let closedTicketPer = dict["close_ticket"] as! Int
        
        self.lblNewTickets.text = String(format: "%.2d %%", newTicketPer)
        self.lblNewTicketsCenter.text = String(format: "%.2d %%", newTicketPer)
        self.lblOpenTickets.text = String(format: "%.2d %%", openTicketPer)
        self.lblClosedTickets.text = String(format: "%.2d %%", closedTicketPer)
        
        
        self.NewTicketsCircle.progress = Double(newTicketPer) / 100
        self.OpenTicketsCircle.progress = Double(openTicketPer) / 100
        self.ClosedTicketsCircle.progress = Double(closedTicketPer) / 100
        
    }
    
    func refreshTickets() {
        let param:[String:Any] = ["id":getID(),
                                  "search":"",
                                  "status":"",
                                  "period":""]
        self.MakeAPICallforTicketsList(param)
    }
    
    func setTableData(){
        internalData = [WrapperObject.init(header: HeaderObject.init(id: "In progress", isOpen: false), listObject: arrInprogressTickets),
                        WrapperObject.init(header: HeaderObject.init(id: "On Hold", isOpen: true), listObject: arrHoldTickets),
                        WrapperObject.init(header: HeaderObject.init(id: "Closed", isOpen: false), listObject: arrClosedTickets)]
        self.tableTickets.reloadData()
    }
    
    //MARK: - API
    
    func MakeAPICallforDeleteTicket(_ param:[String:Int]) {
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_DeleteTicket, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                            let param:[String:Any] = ["id":getID(),
                                                      "search":"",
                                                      "status":"",
                                                      "period":""]
                            self.MakeAPICallforTicketsList(param)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforTicketsList(_ params: [String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_TicketPage, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let msg = data["message"] as? String
                                    {
                                        self.view.makeToast(msg)
                                    }
                                    
                                    
                                    if let inProgress = data["status"] as? [[String:Any]]{
                                        for i in 0..<inProgress.count {
                                            let dic = inProgress[i]
                                            
                                            let status = dic["status"] as? String
                                            let ticket = dic["tickets"] as? [[String:Any]]
                                            
                                            if(status!.caseInsensitiveCompare("in progress") == .orderedSame){
                                                self.arrInprogressTickets = TicketsSuperModel.init(ticket!).tickets
                                            }
                                            else if(status!.caseInsensitiveCompare("hold") == .orderedSame){
                                                self.arrHoldTickets = TicketsSuperModel.init(ticket!).tickets
                                            }
                                            else if(status!.caseInsensitiveCompare("close") == .orderedSame){ 
                                                self.arrClosedTickets = TicketsSuperModel.init(ticket!).tickets
                                            }
                                        }
                                    }
                                    self.setTableData()
                                    
                                    if let tickets = data["ticket"] as? [String:Any]{
                                        if let ticketData = tickets["data"] as? [[String:Any]]{
                                            self.arrTicketsMore = TicketsSuperModel.init(ticketData).tickets
                                            self.height_tableviewMore.constant = CGFloat(self.arrTicketsMore.count * 65)
                                            self.tableMoreTickets.reloadData()
                                        }
                                    }
                                    if let analytics = data["analytics"] as? NSDictionary{
                                        
                                        self.analytics = analytics
                                        self.setDataOfChart(analytics)
                                    }
                                    if self.arrTicketsMore.count == 0 {
                                        self.view_empty.isHidden = false
                                        self.lbl_noDataFound.isHidden = false
                                    } else {
                                        self.view_empty.isHidden = true
                                        self.lbl_noDataFound.isHidden = true
                                    }
                                }
                            }
                        }
                        
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickMenuBtn(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Tickets"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }
    
    @IBAction func onClickNewTicket(_ sender: Any){
        let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor = .clear
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    @objc func headerTapped(_ sender: UIButton){
        var header = self.internalData[sender.tag].header
        
        if header.isOpen{
            header.isOpen = false
        }else{
            header.isOpen = true
        }
        self.internalData[sender.tag].header = header
        
        UIView.animate(withDuration: 0.3) {
            self.tableTickets.reloadData()
        }
    }
}

extension TicketsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableTickets.dequeueReusableHeaderFooterView(withIdentifier: "TicketHeader") as! TicketHeader
        view.setupSegment()
        view.btnTap.tag = section
        view.btnTap.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        view.headerTitle.text = internalData[section].header.id
        return tableView == self.tableTickets ? view : UIView()
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == self.tableTickets ? 60 : 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == self.tableTickets ? 117 : 60
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView == self.tableTickets ? internalData.count : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableTickets{
            if internalData[section].header.isOpen {
                return internalData[section].listObject.count
            } else {
                return 0
            }
            
        }else {
            return arrTicketsMore.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTickets{
            let cell: TicketsListCell = tableView.dequeueReusableCell(withIdentifier: "TicketsListCell") as! TicketsListCell
            if indexPath.section == 0{
                cell.configureCell(arrInprogressTickets[indexPath.row])
            }else if indexPath.section == 1{
                cell.configureCell(arrHoldTickets[indexPath.row])
            }else if indexPath.section == 2{
                cell.configureCell(arrClosedTickets[indexPath.row])
            }
            cell.selectionStyle = .none
            return cell
        }else{
            let cell: TicketMoreCell = tableView.dequeueReusableCell(withIdentifier: "TicketMoreCell", for: indexPath) as! TicketMoreCell
            cell.selectionStyle = .none
            if indexPath.row <= 2{
                //cell.backgroundColor = hexStringToUIColor(hex: "89E065").withAlphaComponent(0.08)
            }else{
                cell.backgroundColor = .clear
                
            }
            let data = self.arrTicketsMore[indexPath.row]
            
            cell.configureCell(arrTicketsMore[indexPath.row])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var ticketModl = TicketsModel()
        //        self.height_ticketTableview.constant = CGFloat()
        if tableView == tableTickets {
            if indexPath.section == 0 {
                if arrInprogressTickets.count > 0{
                    ticketModl = arrInprogressTickets[indexPath.row]
                }
            }
            else if indexPath.section == 1{
                if arrHoldTickets.count > 0{
                    ticketModl = arrHoldTickets[indexPath.row]
                }
            }
            else if indexPath.section == 2{
                if arrClosedTickets.count > 0{
                    ticketModl = arrClosedTickets[indexPath.row]
                }
            }
        } else {
            ticketModl = arrTicketsMore[indexPath.row]
        }
        
        let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
        vc.isUpdating = true
        vc.ticketInfoToUpdate = ticketModl
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor = .clear
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == tableTickets {
            let replyAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
                // changeAction code
                let vc = ReplyTicketVC(nibName: "ReplyTicketVC", bundle: nil)
                vc.modalPresentationStyle = .overCurrentContext
               
                if indexPath.section == 0 {
                    vc.ticket_id = self!.arrInprogressTickets[indexPath.row].id!
                } else if indexPath.section == 1{
                    vc.ticket_id = self!.arrHoldTickets[indexPath.row].id!
                } else {
                    vc.ticket_id = self!.arrClosedTickets[indexPath.row].id!
                }
                self!.navigationController?.present(vc, animated: true, completion:  nil)
                
            }
            replyAction.image = UIImage.init(named: "ic_reply")
            replyAction.title = "Reply"
            replyAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
            
            let deleteaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
                success(true)
                
                if indexPath.section == 0 {
                    let category = self.arrInprogressTickets[indexPath.row]
                    self.tableTickets.reloadData()
                    self.MakeAPICallforDeleteTicket(["id":category.id!])
                    let param:[String:Any] = ["id":getID(),
                                              "search":"",
                                              "status":"",
                                              "period":""]
                    self.MakeAPICallforTicketsList(param)
                    
                }
                else if indexPath.section == 1 {
                    let category = self.arrHoldTickets[indexPath.row]
                    self.tableTickets.reloadData()
                    self.MakeAPICallforDeleteTicket(["id":category.id!])
                    let param:[String:Any] = ["id":getID(),
                                              "search":"",
                                              "status":"",
                                              "period":""]
                    self.MakeAPICallforTicketsList(param)
                }
                else if indexPath.section == 2 {
                    let category = self.arrClosedTickets[indexPath.row]
                    self.tableTickets.reloadData()
                    self.MakeAPICallforDeleteTicket(["id":category.id!])
                    let param:[String:Any] = ["id":getID(),
                                              "search":"",
                                              "status":"",
                                              "period":""]
                    self.MakeAPICallforTicketsList(param)
                }
            }
            deleteaction.image = UIImage.init(named: "ic_delete")
            deleteaction.title = "Delete"
            deleteaction.backgroundColor = hexStringToUIColor(hex: "FC275A")
            
            return UISwipeActionsConfiguration.init(actions: [deleteaction,replyAction])
            
        }
        return UISwipeActionsConfiguration()
    }
}
