//
//  CreateNewTicketVC.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit
import iOSDropDown
import Alamofire


class CreateNewTicketVC: UIViewController {
    @IBOutlet weak var viewAttachments:UIView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCategory: DropDown!
    @IBOutlet weak var txtStatus: DropDown!
    @IBOutlet weak var txtSubjectTitle: UITextField!
    @IBOutlet weak var txtSubject: UITextView!
    @IBOutlet weak var tableFields: UITableView!
    @IBOutlet weak var tableAttachments: UITableView!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    @IBOutlet var tableFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCreateTitle: UILabel!
    @IBOutlet weak var lblNewTicket: UILabel!
    @IBOutlet weak var lblCreateTitleButton : UILabel!
    
    var arrStatus = ["In Progress","On Hold", "Closed"]
    var arrCategories:[categoryModel] = []
    var arrAttachments : [AttachmentItem] = []
    var arrFields : [customFieldModel] = []
    var imagePicker = UIImagePickerController()
    var selectedCatIndex: Int = -1
    var isUpdating: Bool = false
    var ticketInfoToUpdate: TicketsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        viewAttachments.addLineDashedStroke(pattern: [6,4], radius: 10, color: hexStringToUIColor(hex: "DFECFD").cgColor)
        tableFields.register(UINib.init(nibName: "CustomFieldCell", bundle: nil), forCellReuseIdentifier: "CustomFieldCell")
        tableFields.tableFooterView = UIView()
        tableAttachments.register(UINib.init(nibName: "AttachmentTVCell", bundle: nil), forCellReuseIdentifier: "AttachmentTVCell")
        tableAttachments.tableFooterView = UIView()
        
        txtStatus.optionArray = arrStatus
        txtStatus.selectedRowColor = .lightGray
        txtStatus.isSearchEnable = false
        txtStatus.resignFirstResponder()
        txtStatus.didSelect(completion: { selected, _, _ in
            self.txtStatus.text = selected
        })
        
      
        
        self.MakeAPICalltoGetListOfCustomFields(["id":getID()])
        
        let param:[String:Any] = ["id":getID()]
        
        self.MakeAPICalltoGetCategoriesList(param)
        self.checkForUpdating()
    }
    func checkForUpdating(){
        if isUpdating{
            self.setUpdatableData(self.ticketInfoToUpdate)
        }
    }
    
    func setCategoriesData() {
        let temArr = arrCategories.map({$0.name}) as? [String]
        txtCategory.optionArray = temArr!
        txtCategory.selectedRowColor = .lightGray
        txtCategory.isSearchEnable = false
        txtCategory.resignFirstResponder()
        txtCategory.didSelect(completion: { selected, Index, _ in
            self.selectedCatIndex = Index
            self.txtCategory.text = selected
        })
    }
    
    func MakeAPICalltoGetCategoriesList(_ params: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Categories, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                            }
                            if let category = data["category"] as? [[String:Any]]{
                                if category.count == 0 {
                                    self.txtCategory.text = "Test Category"
                                }
                                    self.arrCategories.append(contentsOf: categoriesSuperModel.init(category).categories)
                                    self.setCategoriesData()
                                    self.checkForUpdating()
                                    
                              //  }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func setUpdatableData(_ ticket: TicketsModel){
        self.lblCreateTitle.text = "Update"
        self.lblNewTicket.text = "Update Ticket"
        self.lblCreateTitleButton.text = "Update"
        self.txtFullName.text = ticket.name
        self.txtEmail.text = ticket.email
        self.txtCategory.text = ticket.category
        let catArr = self.arrCategories.map({$0.name})
        if let index = catArr.firstIndex(of: ticket.category!) {
            self.selectedCatIndex = index
        }
        self.txtStatus.text = ticket.status
        self.txtSubjectTitle.text = ticket.subject
        self.txtSubject.text = ticket.desc
        //        self.arrAttachments = ticket.attachments
        
    }
    
    @objc func removeAttachment(_ sender: UIButton){
        arrAttachments.remove(at: sender.tag)
        self.tableAttachments.reloadData()
        self.tableHeight.constant = CGFloat(self.arrAttachments.count) * 62.0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.viewAttachments.layoutIfNeeded()
        }
    }
    
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any)
    {
        var msg: String = ""
        if txtFullName.text! == ""{
            msg = "FullName Field Should Not Empty.!"
        }else if txtEmail.text! == "" {
            msg = "Email Field Should Not Empty.!"
        }else if txtCategory.text! == "" {
            msg = "Category Field Should Not Empty.!"
        }else if txtStatus.text! == ""{
            msg = "Status Field Should Not Empty.!"
        }else if txtSubjectTitle.text! == "" {
            msg = "Subject Title Field Should Not Empty.!"
        }else if txtSubject.text! == ""{
            msg = "Subject Description Field Should Not Empty.!"
        }else if arrAttachments.count == 0 {
            msg = "Please add attachments"
        }else{
            var dic:[String:String] = [:]
            if isUpdating{
                dic = ["id":"\(getID())",
                       "name":txtFullName.text!,
                       "email":txtEmail.text!,
                       "category":"\(arrCategories[selectedCatIndex].id!)",
                       "subject":txtSubjectTitle.text!,
                       "status":txtStatus.text!,
                       "description":txtSubject.text!
                ]
            }else{
                dic = [
                    "name":txtFullName.text!,
                    "email":txtEmail.text!,
                    "category":"1",
                    "subject":txtSubjectTitle.text!,
                    "status":txtStatus.text!,
                    "description":txtSubject.text!
                ]
            }
            self.MakeAPICallforCreateNewTicket(dic)
        }
        self.view.makeToast(msg)
    }
    
    @IBAction func onClickAddField(_ sender: Any)
    {
        //        arrFields.append(1)
        self.tableFields.reloadData()
        
    }
    
    @IBAction func onClickAddAttachment(_ sender: Any)
    {
        ImagePickerManager().pickImage(self){ image in
            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let attachmet = AttachmentItem(imgname: "\(randomString(length: 5)).jpg", imgsize: Double(imageSize), img: image)
            self.arrAttachments.append(attachmet)
            print("actual size of image in KB: %f ", Double(imageSize) / 10000.0)
            self.tableAttachments.reloadData()
            self.tableHeight.constant = CGFloat(self.arrAttachments.count) * 62.0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.viewAttachments.layoutSubviews()
            }
            //here is the image
        }
        
    }
    
    // MARK: - API
    
    func MakeAPICallforCreateNewTicket(_ param:[String:String])
    {
        SHOW_CUSTOM_LOADER()
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param{
                multipartFormData.append((value).data(using: .utf8)!, withName: key)
            }
            for i in 0..<self.arrAttachments.count{
                let atach = self.arrAttachments[i]
                if let jpegData = atach.image.jpegData(compressionQuality: 0.4) {
                    multipartFormData.append(jpegData, withName: "attachments[]", fileName: "image_\(randomString(length: 4)).jpg", mimeType: "image/jpeg")
                }
            }
            for i in 0..<self.arrFields.count{
                let field = self.arrFields[i]
                multipartFormData.append((field.name!).data(using: .utf8)!, withName: "custom_field[\(field.id!)]")
            }
        },
                  to: URL_CreateTicket, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                if response.response?.statusCode == 200 {
                    print("OK. Done")
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? Dictionary<String,Any>
                        {
                            print(jsonArray) // use the json here
                            if let msg = jsonArray["message"] as? String{
                                self.view.makeToast(msg, point: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2), title: "Ticket Created Successfully!", image: nil) { didTap in
                                  
                                        NotificationCenter.default.post(name: Notification.Name("REFRESH_TICKET"), object: nil)
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                            
                            //                            if let data = jsonArray["data"] as? [String:Any]{
                            //
                            //                            }
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }else if response.response?.statusCode == 406{
                    print("error")
                    DispatchQueue.main.sync{
                        let alert = UIAlertController(title: "Error", message:
                                                        "Person has not been set up.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                            DispatchQueue.main.async{
                                HIDE_CUSTOM_LOADER()
                                
                                //                                self.progressUiView.isHidden = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }))
                        
                        self.present(alert, animated: true)
                    }
                }else{
                    print(response.response?.statusCode)
                }
                
                
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        })
    }
    
    func MakeAPICalltoGetListOfCustomFields(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_GetCustomFields, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                        }
                        if let customfields = data["custom_fields"] as? [[String:Any]]
                        {
                            self.arrFields = customFieldsSuperModel.init(customfields).customFields
                            self.tableFields.reloadData()
                            self.tableFieldHeight.constant = CGFloat(self.arrFields.count) * 44.0
                            UIView.animate(withDuration: 0.3) {
                                self.view.layoutIfNeeded()
                            }
                            
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
}

extension CreateNewTicketVC : UITableViewDelegate,  UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == self.tableFields ? arrFields.count : arrAttachments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableFields{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomFieldCell") as! CustomFieldCell
            cell.selectionStyle = .none
            cell.configureCell(arrFields[indexPath.row])
            return cell
        }
        let cell: AttachmentTVCell = tableView.dequeueReusableCell(withIdentifier: "AttachmentTVCell") as! AttachmentTVCell
        cell.selectionStyle = .none
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(removeAttachment), for: .touchUpInside)
        cell.lblAttachmentName.text = arrAttachments[indexPath.row].name
        cell.lblSize.text = String(format: "(%.2f)", arrAttachments[indexPath.row].size)
        return cell
    }
}
