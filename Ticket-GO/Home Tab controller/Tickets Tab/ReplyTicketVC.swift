//
//  CreateNewTicketVC.swift
//  Ticket-GO
//
//  Created by mac on 12/07/23.
//

import UIKit
import iOSDropDown
import Alamofire


class ReplyTicketVC: UIViewController {
    @IBOutlet weak var viewAttachments:UIView!
    @IBOutlet weak var txtSubject: UITextView!
    @IBOutlet weak var tableAttachments: UITableView!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNewTicket: UILabel!
    @IBOutlet weak var lblCreateTitleButton : UILabel!
    @IBOutlet weak var textView_description: UITextView!
    @IBOutlet weak var textView_notes: UITextView!
    
    
    var arrStatus = ["In Progress","On Hold", "Closed"]
    var arrCategories:[categoryModel] = []
    var arrAttachments : [AttachmentItem] = []
    var arrFields : [customFieldModel] = []
    var imagePicker = UIImagePickerController()
    var selectedCatIndex: Int = -1
    var isUpdating: Bool = false
    var ticketInfoToUpdate: TicketsModel!
    var ticket_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        viewAttachments.addLineDashedStroke(pattern: [6,4], radius: 10, color: hexStringToUIColor(hex: "DFECFD").cgColor)
        //tableFields.register(UINib.init(nibName: "CustomFieldCell", bundle: nil), forCellReuseIdentifier: "CustomFieldCell")
        //tableFields.tableFooterView = UIView()
        tableAttachments.register(UINib.init(nibName: "AttachmentTVCell", bundle: nil), forCellReuseIdentifier: "AttachmentTVCell")
        tableAttachments.tableFooterView = UIView()
        
        let param:[String:Any] = ["id":getID()]
        
        //self.MakeAPICalltoGetCategoriesList(param)
//        self.checkForUpdating()
    }
    
    
    @objc func removeAttachment(_ sender: UIButton){
        arrAttachments.remove(at: sender.tag)
        self.tableAttachments.reloadData()
        self.tableHeight.constant = CGFloat(self.arrAttachments.count) * 62.0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.viewAttachments.layoutIfNeeded()
        }
    }
    
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any)
    {
        var msg: String = ""
        
        if textView_description.text! == "" {
            msg = "Description Field Should Not Empty.!"
        }else if arrAttachments.count == 0 {
            msg = "Please add attachments"
        }else{
            var dic:[String:String] = [:]
            dic = [
                "id":"\(getID())",
                "ticket_id":"\(self.ticket_id)",
                "reply_description":textView_description.text!
            ]
            
            self.MakeAPICallforCreateNewTicket(dic)
        }
        self.view.makeToast(msg)
    }
    
    @IBAction func onClickAddField(_ sender: Any)
    {
        //        arrFields.append(1)
        
        
    }
    
    @IBAction func onClickAddAttachment(_ sender: Any)
    {
        ImagePickerManager().pickImage(self){ image in
            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let attachmet = AttachmentItem(imgname: "\(randomString(length: 5)).jpg", imgsize: Double(imageSize), img: image)
            self.arrAttachments.append(attachmet)
            print("actual size of image in KB: %f ", Double(imageSize) / 10000.0)
            self.tableAttachments.reloadData()
            self.tableHeight.constant = CGFloat(self.arrAttachments.count) * 62.0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.viewAttachments.layoutSubviews()
            }
            //here is the image
        }
        
    }
    
    // MARK: - API
    
    func MakeAPICallforCreateNewTicket(_ param:[String:String])
    {
        SHOW_CUSTOM_LOADER()
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param{
                multipartFormData.append((value).data(using: .utf8)!, withName: key)
            }
            for i in 0..<self.arrAttachments.count{
                let atach = self.arrAttachments[i]
                if let jpegData = atach.image.jpegData(compressionQuality: 0.4) {
                    multipartFormData.append(jpegData, withName: "reply_attachments[]", fileName: "image_\(randomString(length: 4)).jpg", mimeType: "image/jpeg")
                }
            }
            //  for i in 0..<self.arrFields.count{
            //      let field = self.arrFields[i]
            //       multipartFormData.append((field.name!).data(using: .utf8)!, withName: "custom_field[\(field.id!)]")
            //}
        },
                  to: URL_ReplyTicket, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                if response.response?.statusCode == 200 {
                    print("OK. Done")
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? Dictionary<String,Any>
                        {
                            print(jsonArray) // use the json here
                            if let msg = jsonArray["message"] as? String{
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                            //                            if let data = jsonArray["data"] as? [String:Any]{
                            //
                            //                            }
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }else if response.response?.statusCode == 406{
                    print("error")
                    DispatchQueue.main.sync{
                        let alert = UIAlertController(title: "Error", message:
                                                        "Person has not been set up.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                            DispatchQueue.main.async{
                                HIDE_CUSTOM_LOADER()
                                
                                //                                self.progressUiView.isHidden = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }))
                        
                        self.present(alert, animated: true)
                    }
                }else{
                    print(response.response?.statusCode)
                }
                
                
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        })
    }
}

extension ReplyTicketVC : UITableViewDelegate,  UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAttachments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AttachmentTVCell = tableView.dequeueReusableCell(withIdentifier: "AttachmentTVCell") as! AttachmentTVCell
        cell.selectionStyle = .none
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(removeAttachment), for: .touchUpInside)
        cell.lblAttachmentName.text = arrAttachments[indexPath.row].name
        cell.lblSize.text = String(format: "(%.2f)", arrAttachments[indexPath.row].size)
        return cell
    }
}
