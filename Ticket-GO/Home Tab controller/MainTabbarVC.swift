//
//  MainTabbarVC.swift
//  Ticket-GO
//
//  Created by mac on 23/06/22.
//

import UIKit

class MainTabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selFont = UIFont.init(name: "Outfit-Semibold", size: 10)
        let normalFont = UIFont.init(name: "Outfit-Medium", size: 10)

       
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:normalFont, .foregroundColor: UIColor.black]
        
        let AttriSelected = [NSAttributedString.Key.font:selFont, .foregroundColor: UIColor.white]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        appearance.setTitleTextAttributes(AttriSelected as [NSAttributedString.Key : Any], for: .selected)
        
//        tabBar.backgroundColor = UIColor.white

        // Removing the upper border of the UITabBar.
        //
        // Note: Don't use `tabBar.clipsToBounds = true` if you want
        // to add a custom shadow to the `tabBar`!
        //
        if #available(iOS 13, *) {
            // iOS 13:
            let appearance = tabBar.standardAppearance
            appearance.configureWithOpaqueBackground()
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
        } else {
            // iOS 12 and below:
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "tab_notification"), object: nil)


        // Do any additional setup after loading the view.
    }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {

      if let indx = notification.userInfo?["tab"] as? Int {
          self.selectedIndex = indx
      // do something with your image
      }
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
