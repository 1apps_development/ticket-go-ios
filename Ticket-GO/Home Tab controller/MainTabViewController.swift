//
//  MainTabViewController.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import SOTabBar

//172C4E

class MainTabViewController: SOTabBarController {
    
    override func loadView() {
        super.loadView()
        SOTabBarSetting.tabBarHeight = 65
        SOTabBarSetting.tabBarBackground = hexStringToUIColor(hex: "172C4E")
        SOTabBarSetting.tabBarTintColor = #colorLiteral(red: 0.4352941176, green: 0.8509803922, blue: 0.262745098, alpha: 1)
        SOTabBarSetting.tabBarCircleSize = CGSize(width: 50, height: 50)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        let tasksVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TicketsVC")
        let invoicesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesVC")
        let dashbordvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardVC")
        let usersvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UsersVC")
//        let trackervc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrackerViewController")
        let settingsvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC")
       
        let image = UIImage.init(named: "tab_tickets")?.imageWithColor(color1: hexStringToUIColor(hex: "717887"))
        
        tasksVC.tabBarItem = UITabBarItem(title: "Tickets", image: image, selectedImage: UIImage(named: "tab_tickets_sel"))
        invoicesVC.tabBarItem = UITabBarItem(title: "Categories", image: UIImage(named: "tab_categories"), selectedImage: UIImage(named: "tab_categorie_sel"))
        dashbordvc.tabBarItem = UITabBarItem(title: "Dashbord", image: UIImage(named: "tab_dash"), selectedImage: UIImage(named: "tab_dash_sel"))
        usersvc.tabBarItem = UITabBarItem(title: "Users", image: UIImage(named: "tab_users"), selectedImage: UIImage(named: "tab_user_sel"))
        settingsvc.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "tab_settings"), selectedImage: UIImage(named: "tab_setting_sel"))
//        trackervc.tabBarItem = UITabBarItem(title: "Tracker", image: UIImage(named: "tab_tracker"), selectedImage: UIImage(named: "clock"))

           
        viewControllers = [tasksVC, invoicesVC,dashbordvc,usersvc,settingsvc]
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "tab_notification"), object: nil)

    }
   @objc func showSpinningWheel(_ notification: NSNotification) {

     if let indx = notification.userInfo?["tab"] as? Int {
//         self.tabBar(SOTabBar(), didSelectTabAt: indx)

     // do something with your image
     }
    }

    
}

extension MainTabViewController: SOTabBarControllerDelegate {
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
//        print(viewController.tabBarItem.title ?? "")
    }
}
