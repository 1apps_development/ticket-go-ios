//
//  CategoriesVC.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit
import Alamofire
import Toast_Swift

class CategoriesVC: UIViewController {
    @IBOutlet weak var tableCategories: UITableView!
    @IBOutlet weak var tableCategoriesMore: UITableView!
    @IBOutlet var heightTableMore: NSLayoutConstraint!
    @IBOutlet weak var lblCategoryNameCenter: UILabel!
    @IBOutlet weak var lblPercentageCenter: UILabel!
    @IBOutlet weak var lbl_date_corner: UILabel!
    @IBOutlet weak var circle1: KDCircularProgress!
    @IBOutlet weak var circle2: KDCircularProgress!
    @IBOutlet weak var circle3: KDCircularProgress!
    @IBOutlet weak var view_empty: UIView!
    @IBOutlet weak var lbl_noDataFound: UILabel!
    
    @IBOutlet weak var height_tableview: NSLayoutConstraint!
    var arrCategories : [categoryModel] = []
    var arrCategoriesMore : [categoryMoreModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_date_corner.text = getCurrentDate()
        self.view_empty.isHidden = false
        tableCategories.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        tableCategories.tableFooterView = UIView()
        tableCategoriesMore.register(UINib(nibName: "CategoryMoreCell", bundle: nil), forCellReuseIdentifier: "CategoryMoreCell")
        tableCategoriesMore.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterupdate), name: Notification.Name("REFRESH_VIEW"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MakeAPICalltoGetCategoriesList()

    }
    
    func setProgressData()
    {
        if arrCategoriesMore.count > 2{
            let firstObject = arrCategoriesMore[0].value
            let secondObject = arrCategoriesMore[1].value
            let thirdObject = arrCategoriesMore[2].value
            
            self.lblPercentageCenter.text = String(format: "%.2d %%", firstObject!)
            self.lblCategoryNameCenter.text = arrCategoriesMore[0].category!
            
            self.circle1.progress = Double(firstObject!) / 100
            self.circle2.progress = Double(secondObject!) / 100
            self.circle3.progress = Double(thirdObject!) / 100
            
            self.circle1.progressColors = [hexStringToUIColor(hex: arrCategoriesMore[0].color!)]
            self.circle2.progressColors = [hexStringToUIColor(hex: arrCategoriesMore[1].color!)]
            self.circle3.progressColors = [hexStringToUIColor(hex: arrCategoriesMore[2].color!)]
            
        }
        
    }
    
    @objc func refreshViewAfterupdate(){
        self.MakeAPICalltoGetCategoriesList()
    }
    
    //MARK: - IBAction
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Categories"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }
    
    @IBAction func onClickCreateCategory(_ sender: Any){
        let vc = CreateNewCategoryVC(nibName: "CreateNewCategoryVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion:  nil)
    }
    
    //MARK: - Custom Functions
    
    //MARK: - API
    
    func MakeAPICalltoGetCategoriesList(){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            ServiceManager.callGetApi(URL_GetCategories, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                            }
                            if let status = json["status"] as?  Int{
                                if status == 401{
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        appDelegate.logoutWithoutAPI()
                                    }
                                    break
                                }else if status == 1{
                                    if let category = data["category"] as? [String:Any]{
                                        if let categories = category["data"] as? [[String:Any]]
                                        {
                                            self.arrCategories = categoriesSuperModel.init(categories).categories
                                            var tmpArr:[String] = []
                                            for i in 0..<categories.count{
                                                let dic = categories[i]
                                                if let name = dic["name"] as? String{
                                                    tmpArr.append(name)
                                                }
                                            }
                                            if self.arrCategories.count == 0 {
                                                self.view_empty.isHidden = false
                                                self.lbl_noDataFound.isHidden = false
                                            } else {
                                                self.view_empty.isHidden = true
                                                self.lbl_noDataFound.isHidden = true
                                            }
                                            self.tableCategories.reloadData()
                                            self.height_tableview.constant = CGFloat(self.arrCategories.count * 60)
                                        }
                                        if let categoryAnalysis = data["category_analytics"] as? [[String:Any]]{
                                            self.arrCategoriesMore = categoriesSuperModel.init(categoryAnalysis).categoriesMore
                                            self.setProgressData()
                                            self.tableCategoriesMore.reloadData()
                                            self.heightTableMore.constant = CGFloat(self.arrCategoriesMore.count * 60)
                                            self.view.layoutIfNeeded()
                                        }
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallforDeleteCategory(_ param:[String:Int])
    {
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_DeleteCategory, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                            self.MakeAPICalltoGetCategoriesList()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
}



extension CategoriesVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == self.tableCategories ? arrCategories.count : arrCategoriesMore.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableCategories{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
            cell.selectionStyle = .none
            cell.configureCell(arrCategories[indexPath.row])
            return cell
        }else{
            let cel = tableView.dequeueReusableCell(withIdentifier: "CategoryMoreCell") as! CategoryMoreCell
            cel.selectionStyle = .none
            cel.configureCell(arrCategoriesMore[indexPath.row])
            return cel
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == self.tableCategories{
            let changeAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
                // changeAction code
                let vc = CreateNewCategoryVC(nibName: "CreateNewCategoryVC", bundle: nil)
                vc.modalPresentationStyle = .overCurrentContext
                vc.isUpdating = true
                vc.category = self!.arrCategories[indexPath.row]
                self!.navigationController?.present(vc, animated: true, completion:  nil)
                
            }
            
            let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
                // deleteAction code
                if IS_DEMO_MODE == false {
                    let category = self!.arrCategories[indexPath.row]
                    self?.MakeAPICallforDeleteCategory(["id":category.id!])
                } else {
                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
                }
                
            }
            // Set the button's images
            changeAction.image = UIImage.init(named: "ic_adjust")
            changeAction.title = "Change"
            deleteAction.image = UIImage.init(named: "ic_delete")
            deleteAction.title = "Delete"
            changeAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
            deleteAction.backgroundColor = hexStringToUIColor(hex: "FC275A")
            
            return UISwipeActionsConfiguration.init(actions: [deleteAction,changeAction])
        }
        return UISwipeActionsConfiguration()
    }
    
}
