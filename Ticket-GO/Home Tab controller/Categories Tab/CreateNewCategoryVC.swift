//
//  CreateNewCategoryVC.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit
import Alamofire

class CreateNewCategoryVC: UIViewController {
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblColorCode: UILabel!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewColor: TelegramColorPicker!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblNewCategory: UILabel!
    @IBOutlet weak var lblCreateButton: UILabel!
    
    var selectedColor: String!
    var isUpdating:Bool = false
    var category: categoryModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDone.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnDone.layer.shadowColor = UIColor.darkGray.cgColor
        btnDone.layer.shadowOpacity = 0.7
        btnDone.layer.shadowRadius = 8
        viewColor.layer.borderWidth = 0.8
        viewColor.layer.borderColor = UIColor.lightGray.cgColor
        viewColor.layer.cornerRadius = 10
        viewColor.getColorUpdate { [weak self] (_, color) in
            guard let newColor = color.newValue, let hexadecimalColor = newColor.toHex() else { return }
            self?.viewBG.backgroundColor = newColor
            self?.selectedColor = hexadecimalColor
            self?.lblColorCode.text = hexadecimalColor
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.isUpdating{
            self.setEditableData()
        }
    }
    
    func setEditableData(){
        self.lblNewCategory.text = "Edit Category"
        self.lblCreateButton.text = "Update"
        self.lblCreate.text = "Edit"
        self.viewBG.backgroundColor = hexStringToUIColor(hex: category.color!)
        self.lblColorCode.text = category.color!
        self.txtFullname.text = category.name!
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCloseColor(_ sender: Any)
    {
        self.viewColor.isHidden = true
        self.btnDone.isHidden = true
    }
    
    @IBAction func onClickColorPicker(_ sender: Any)
    {
        self.btnDone.isHidden = false
        self.viewColor.isHidden = false
    }
    
    @IBAction func onClickCreateCategory(_ sender: Any)
    {
        if txtFullname.text! == ""{
            self.view.makeToast("Please enter your Name")
        }else{
            if self.isUpdating == true {
                let param:[String:Any] = ["id":category.id!,
                                          "name":txtFullname.text!,
                                          "color":selectedColor!]
                self.MakeAPICallforCreateCategory(param)
            } else {
                let param:[String:Any] = ["user_id":getID(),
                                          "name":txtFullname.text!,
                                          "color":selectedColor!]
                self.MakeAPICallforCreateCategory(param)
            }
            
        }
    }
    
    //MARK: - API
    
    func MakeAPICallforCreateCategory(_ dict:[String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            
            AIServiceManager.sharedManager.callPostApi(URL_CreateCategory, params: dict, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        
                        if let msg = json["message"] as? String
                        {
                            if msg == "successfull"{
                                //if self.isUpdating{
                                NotificationCenter.default.post(name: Notification.Name("REFRESH_VIEW"), object: nil)
                                //}
                                self.dismiss(animated: true, completion: { })
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
}
