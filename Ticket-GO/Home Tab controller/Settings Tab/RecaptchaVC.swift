//
//  RecaptchaVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit
import Alamofire

class RecaptchaVC: UIViewController {
    
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var txtGoogleKey: UITextField!
    @IBOutlet weak var txtGoogleSecret: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnGoogle.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnGoogle.setImage(UIImage.init(named: "switch_off"), for: .normal)

        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickGoogle(_ sender: Any){
        btnGoogle.isSelected = btnGoogle.isSelected == true ? false : true
    }
    
    @IBAction func onClickUpdate(_ sender: Any){
        var msg:String = ""
        if txtGoogleKey.text! == ""{
            msg = "Google Recaptcha Key should not Empty"
        }else if txtGoogleSecret.text! == ""{
            msg = "Google Recaptcha Secret should not Empty"
        }else{
            let param:[String:Any] = ["id":getID(),
                                      "recaptcha_module":btnGoogle.isSelected == true ? "on" : "off",
                                      "google_recaptcha_key":txtGoogleKey.text!,
                                      "google_recaptcha_secret":txtGoogleSecret.text!]
            self.MakeAPICallforUpdateRecaptcha(param)
        }
        self.view.makeToast(msg)
    }
    
    //MARK: - API

    func MakeAPICallforUpdateRecaptcha(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_RecaptchaSettings, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let msg = json["message"] as? String{
                                    self.view.makeToast(msg)
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
