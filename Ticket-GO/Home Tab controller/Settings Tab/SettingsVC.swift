//
//  SettingsVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet weak var tableSettings: UITableView!
    
    var arrSettings = [SettingItem(title: "Site Settings", image: "set_site"),
                       //SettingItem(title: "Email Settings", image: "set_email"),
                       SettingItem(title: "Recaptcha Settings", image: "set_recaptcha"),
                       SettingItem(title: "Ticket Fields", image: "set_ticket"),
                       SettingItem(title: "Logout", image: "set_logout")]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableSettings.register(UINib.init(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        tableSettings.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func openViewControllerWithName(name: String){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Settings"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
//        self.navigationController?.popViewController(animated: true)
    }

}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.selectionStyle = .none
        cell.configureCellWith(arrSettings[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = arrSettings[indexPath.row].title
        var vcName : String = ""
        if title == "Site Settings"{
            vcName = "SiteSettingsViewController"
        }else if title == "Email Settings"{
            vcName = "EmailSettingsVC"
        }else if title == "Ticket Fields"{
            vcName = "TicketFiledVC"
        }else if title == "Recaptcha Settings"{
            vcName = "RecaptchaVC"
        }else if title == "Logout"{
            displayAlertWithTitle(self, title: "Confirm", andMessage: "Are you really want to logout?", buttons: ["NO","LOGOUT"]) { index in
                if index == 1{
                    appDelegate.logoutWithoutAPI(true)
                }
            }
        }
        if vcName != ""{
            self.openViewControllerWithName(name: vcName)
        }

    }
}
