//
//  UsersVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import Alamofire

class UsersVC: UIViewController {
    
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet weak var viewShowMore: UIView!
    @IBOutlet weak var lbl_date_corner : UILabel!
    @IBOutlet weak var lbl_noDataFound: UILabel!
    @IBOutlet weak var btn_showMore: UIButton!
    
    var arrUsers: [userModel] = []
    var nextPageUrl: String = ""
    var currentPage:Int = 0
    var lastPage: Int = 0
    var defaultParam :[String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_date_corner.text = getCurrentDate()
        self.lbl_noDataFound.isHidden = false
        self.btn_showMore.isHidden = false
        tableUsers.register(UINib.init(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCell")
        tableUsers.tableFooterView = UIView()
        self.refreshUsersList(true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshUsersList(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name("REFRESH_USERS"), object: nil)
    }
    
    //MARK: - Custom Functions
    
    @objc func notificationReceived(_ notification: NSNotification){
        if let isnew = notification.userInfo?["isUpdated"] as? Bool {
            if isnew{
                self.refreshUsersList(isnew)
            }
        }
    }
    
    @objc func refreshUsersList(_ isNewUser:Bool = true){
        if isNewUser{
            self.arrUsers.removeAll()
            self.currentPage = 1
        }
        defaultParam = ["id":getID(),
                        "page":self.currentPage,
                        "per_page":1,
                        "search":""]
        self.MakeAPICallforUsersList(defaultParam)
    }
    
    
    func loadMoreItemsForList(){
        if currentPage >= lastPage{
            //            self.viewShowMore.isHidden = true
        }else{
            //            self.viewShowMore.isHidden = false
            currentPage += 1
            self.refreshUsersList(false)
        }
    }
    
    
    
    //MARK: - IBActions
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Users"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
        
    }
    @IBAction func onClickNewUser(_ sender: Any){
        let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
        
    }
    @IBAction func onClickShowMore(_ sender: Any)
    {
        self.loadMoreItemsForList()
        
    }
    
    //MARK: - API
    
    func MakeAPICallforUsersList(_ param: [String:Any]){
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_GetUsersList, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as?  Int{
                        if status == 401{
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                appDelegate.logoutWithoutAPI()
                            }
                            break
                        }else if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let users = data["users"] as? [String:Any]{
//                                    if users.count > 0 {\
                                        self.currentPage = users["current_page"] as! Int
                                        self.lastPage = users["last_page"] as! Int

                                    if let arrUsers = users["data"] as? [[String:Any]] {
                                            self.arrUsers = usersSuperModel.init(arrUsers).users
                                            self.tableUsers.reloadData()
                                        }
                                    if self.arrUsers.count > 0 {
                                        DispatchQueue.main.async {
                                            if self.arrUsers.count > 0 {
                                                let indexPath = IndexPath(row: self.arrUsers.count-1, section: 0)
                                                self.tableUsers.scrollToRow(at: indexPath, at: .bottom, animated: true)
                                            }
                                        }
                                        self.lbl_noDataFound.isHidden = true
                                        self.btn_showMore.isHidden = true
                                    }
                                    else {
                                        self.lbl_noDataFound.isHidden = false
                                        self.btn_showMore.isHidden = false
                                    }
                                   // }
                                    //else {
                                        
                                    //}
                                }
                            }
                            printD(json)
                        }
                    }
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforDeleteUser(_ param:[String:Any])
    {
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_DeleteUser, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            self.refreshUsersList()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
}

extension UsersVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UsersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell") as! UsersTableViewCell
        cell.selectionStyle = .none
        cell.configureCell(arrUsers[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let changeAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // changeAction code
            
            let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
            vc.isUpdating = true
            vc.userInfo = self!.arrUsers[indexPath.row]
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self!.navigationController?.present(vc, animated: true)
            
        }
        
        let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // deleteAction code
            if IS_DEMO_MODE == true {
                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
            } else {
                let user = self!.arrUsers[indexPath.row]
                self?.MakeAPICallforDeleteUser(["id":user.id!])
            }
        }
        // Set the button's images
        changeAction.image = UIImage.init(named: "ic_adjust")
        changeAction.title = "Change"
        deleteAction.image = UIImage.init(named: "ic_delete")
        deleteAction.title = "Delete"
        changeAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
        deleteAction.backgroundColor = hexStringToUIColor(hex: "FC275A")
        
        return UISwipeActionsConfiguration.init(actions: [deleteAction,changeAction])
    }
}
