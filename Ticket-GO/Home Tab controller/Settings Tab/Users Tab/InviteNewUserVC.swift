//
//  InviteNewUserVC.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit
import iOSDropDown
import Alamofire

class InviteNewUserVC: UIViewController {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCnfPassword: UITextField!
    @IBOutlet weak var txtRole: DropDown!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblInvite: UILabel!
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblNewuser: UILabel!
    
    var arrRoles:[rolesModel] = []
    //var selRoleIndex: Int = -1
    var isUpdating:Bool = false
    var userInfo: userModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Functions
    
    func setupInitialUI(){
        //MakeAPICallforRole()
        
        txtRole.selectedRowColor = .lightGray
        txtRole.isSearchEnable = false
        self.txtRole.optionArray = ["Agent"]
        txtRole.didSelect(completion: { selected, index, _ in
            self.txtRole.text = selected
            //            self.selRoleIndex = index
        })
        if self.isUpdating{
            self.lblCreate.text = "Edit"
            self.lblInvite.text = "Update"
            self.lblNewuser.text = "Edit User"
            
            self.MakeAPICallforGetUser(["id":self.userInfo.id!])
        }
        
    }
    
    func setUsersData(_ user: userModel)
    {
        self.lblImageName.text = user.avtar
        self.txtName.text = user.name
        self.txtEmail.text = user.email
        self.txtRole.text = user.role
        if let imgUrl = URL(string: user.avtar!){
            self.userImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
    
    // MARK: - IBActions
    
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any){
        var msg:String = ""
        
        if txtName.text! == ""{
            msg = "FullName Field Should Not Empty.!"
        }else if txtEmail.text! == ""{
            msg = "Email Field Should Not Empty.!"
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            msg = "Please enter valid Email Address"
        }else if txtRole.text! == "" {
            msg = "Role Field Should Not Empty.!"
        }else if txtPassword.text! == ""{
            msg = "Password Field Should Not Empty.!"
        }else if txtCnfPassword.text! == ""{
            msg = "Confirm Password Field Should Not Empty.!"
        }else if txtPassword.text! != txtCnfPassword.text{
            msg = "Password and Confirm Password not Matched.!"
        }else if lblImageName.text! == ""{
            msg = "Please add an Attachment.!"
        }else{
            var param:[String:String] = [:]
            if isUpdating{
                param = ["id":"\(self.userInfo.id!)",
                         "name":txtName.text!,
                         
                         "role":"2",
                         "email":txtEmail.text!,
                         "password":txtPassword.text!,
                         "password_confirmation":txtCnfPassword.text!]
            }else{
                
                param = [//"id":"\(getID())",
                    "name":txtName.text!,
                    "role":"2",
                    "email":txtEmail.text!,
                    "password":txtPassword.text!,
                    "password_confirmation":txtCnfPassword.text!]
            }
            if IS_DEMO_MODE == true && isUpdating == true {
                self.view.makeToast("You can't access this functionality as a demo user")
            } else {
                self.MakeAPICallforCreateNewUser(param)
            }
            
        }
        if msg != "" {
            self.view.makeToast(msg)
        }
        
    }
    @IBAction func onClickImage(_ sender: Any)
    {
        ImagePickerManager().pickImage(self){ image in
            self.userImage.image = image
            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let attachmet = AttachmentItem(imgname: "\(randomString(length: 5)).jpg", imgsize: Double(imageSize), img: image)
            self.lblImageName.text = "img_\(randomString(length: 5)).png"
            print("actual size of image in KB: %f ", Double(imageSize) / 10000.0)
            //here is the image
        }
    }
    
    // MARK: - API
    
    func MakeAPICallforGetUser(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_GetUser, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let user = data["users"] as? [String:Any]{
                                    let userModel = userModel.init(user)
                                    self.setUsersData(userModel)
                                }
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforRole()
    {
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callGetApi(URL_GetRole, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                        }
                        if let roles = data["role"] as? [[String:Any]]{
                            //                            self.arrRoles = rolesSuperModel.init(roles).roles
                            self.arrRoles.append(contentsOf: rolesSuperModel.init(roles).roles)
                            var tmpArr : [String] = []
                            for i in 0..<roles.count{
                                let dic = roles[i]
                                if let name = dic["name"] as? String{
                                    tmpArr.append(name)
                                }
                            }
                            self.txtRole.optionArray = tmpArr
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforCreateNewUser(_ param:[String:String])
    {
        SHOW_CUSTOM_LOADER()
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param{
                multipartFormData.append((value).data(using: .utf8)!, withName: key)
            }
            if let jpegData = self.userImage.image!.jpegData(compressionQuality: 0.4) {
                multipartFormData.append(jpegData, withName: "avatar", fileName: "image_\(randomString(length: 4)).jpg", mimeType: "image/jpeg")
            }
        },
                  to: URL_CreateUser, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                if response.response?.statusCode == 200 {
                    print("OK. Done")
                    do {
                        
                        if let jsonDict = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? NSDictionary
                        {
                            print(jsonDict) // use the json here
                            if let msg = jsonDict["message"] as? String{
                                let imageDataDict:[String: Bool] = ["isUpdated": true]
                                
                                NotificationCenter.default.post(name: Notification.Name("REFRESH_USERS"), object: nil, userInfo: imageDataDict)
                                self.dismiss(animated: true, completion: nil)
                                
                                //                                self.view.makeToast(msg, point: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2), title: "User Created Successfully!", image: nil) { didTap in
                                //                                }
                            }
                            //                            if let data = jsonArray["data"] as? [String:Any]{
                            //
                            //                            }
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }else if response.response?.statusCode == 406{
                    print("error")
                    DispatchQueue.main.sync{
                        let alert = UIAlertController(title: "Error", message:
                                                        "Person has not been set up.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                            DispatchQueue.main.async{
                                HIDE_CUSTOM_LOADER()
                                
                                //                                self.progressUiView.isHidden = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }))
                        
                        self.present(alert, animated: true)
                    }
                }else{
                    print(response.response?.statusCode)
                }
                
                
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        })
    }
}
