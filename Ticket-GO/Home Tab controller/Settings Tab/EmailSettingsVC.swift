//
//  EmailSettingsVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit
import Alamofire

class EmailSettingsVC: UIViewController {
    @IBOutlet weak var txtMailDriver: UITextField!
    @IBOutlet weak var txtMailUsername: UITextField!
    @IBOutlet weak var txtMailFromAddress: UITextField!
    @IBOutlet weak var txtMailHost: UITextField!
    @IBOutlet weak var txtMailPassword: UITextField!
    @IBOutlet weak var txtMailFromName: UITextField!
    @IBOutlet weak var txtMailPort: UITextField!
    @IBOutlet weak var txtMailEncryption: UITextField!
    @IBOutlet weak var txtMystore: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        // Do any additional setup after loading the view.
    }
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        self.MakeAPICallforEmailSettingsPage(["user_id":getID()])
    }
    
    func setEmailSettingsDefaultData(_ dict:[String:Any])
    {
        if let mailDriver = dict["mail_driver"] as? String{
            self.txtMailDriver.text = mailDriver
        }
        if let mailHost = dict["mail_host"] as? String{
            self.txtMailHost.text = mailHost
        }
        if let mailPort = dict["mail_port"] as? String{
            self.txtMailPort.text = mailPort
        }
        if let mailUname = dict["mail_username"] as? String{
            self.txtMailUsername.text = mailUname
        }
        if let mailPass = dict["mail_password"] as? String{
            self.txtMailPassword.text = mailPass
        }
        if let mailEncry = dict["mail_encryption"] as? String{
            self.txtMailEncryption.text = mailEncry
        }
        if let mailFromAdd = dict["mail_from_address"] as? String{
            self.txtMailFromAddress.text = mailFromAdd
        }
        if let mailFromNam = dict["mail_from_name"] as? String{
            self.txtMailFromName.text = mailFromNam
        }
    }

    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSaveChanges(_ sender: Any){
        var msg: String = ""
        if txtMailDriver.text! == ""{
            msg = "Mail Driver field should not empty."
        }else if txtMailUsername.text! == ""{
            msg = "Mail Username field should not empty."
        }else if txtMailFromAddress.text! == ""{
            msg = "Mail From Address field should not empty."
        }else if txtMailHost.text! == ""{
            msg = "Mail Host field should not empty."
        }else if txtMailPassword.text! == ""{
            msg = "Mail Password field should not empty."
        }else if txtMailFromName.text! == ""{
            msg = "Mail From Name field should not empty."
        }else if txtMailPort.text! == ""{
            msg = "Mail Port field should not empty."
        }else if txtMailEncryption.text! == ""{
            msg = "Mail Encryption field should not empty."
        }else if txtMystore.text! == ""{
            msg = "My Store field should not empty."
        }else{
            let param:[String:Any] = ["id":getID(),
                                      "mail_driver":txtMailDriver.text!,
                                      "mail_host":txtMailHost.text!,
                                      "mail_port":txtMailPort.text!,
                                      "mail_username":txtMailUsername.text!,
                                      "mail_password":txtMailPassword.text!,
                                      "mail_encryption":txtMailEncryption.text!,
                                      "mail_from_address":txtMailFromAddress.text!,
                                      "mail_from_name":txtMailFromName.text!]
            self.MakeAPICallforUpdateEmailSettings(param)
        }
        self.view.makeToast(msg)
        
    }
    
    @IBAction func onClickSendTestMail(_ sender: Any){
        var msg: String = ""
        if txtMailDriver.text! == ""{
            msg = "Mail Driver field should not empty."
        }else if txtMailUsername.text! == ""{
            msg = "Mail Username field should not empty."
        }else if txtMailFromAddress.text! == ""{
            msg = "Mail From Address field should not empty."
        }else if txtMailHost.text! == ""{
            msg = "Mail Host field should not empty."
        }else if txtMailPassword.text! == ""{
            msg = "Mail Password field should not empty."
        }else if txtMailFromName.text! == ""{
            msg = "Mail From Name field should not empty."
        }else if txtMailPort.text! == ""{
            msg = "Mail Port field should not empty."
        }else if txtMailEncryption.text! == ""{
            msg = "Mail Encryption field should not empty."
        }else if txtMystore.text! == ""{
            msg = "My Store field should not empty."
        }else{
            //Variable to store alertTextField
            var textField = UITextField()
            
            let alert = UIAlertController(title: "Send Test Mail", message: "", preferredStyle: .alert)
            alert.addTextField { alertTextField in
                alertTextField.placeholder = "User Email Address"
                
                //Copy alertTextField in local variable to use in current block of code
                textField = alertTextField
            }
            
            let action = UIAlertAction(title: "Send", style: .default) { action in
                //Prints the alertTextField's value
                let param:[String:Any] = [
                    "user_mail":textField.text!,
                    "mail_driver":self.txtMailDriver.text!,
                    "mail_host":self.txtMailHost.text!,
                    "mail_port":self.txtMailPort.text!,
                    "mail_username":self.txtMailUsername.text!,
                    "mail_password":self.txtMailPassword.text!,
                    "mail_encryption":self.txtMailEncryption.text!,
                    "mail_from_address":self.txtMailFromAddress.text!,
                    "mail_from_name":self.txtMailFromName.text!]
                self.MakeAPICallforSendTestMail(param)
                
            }
            let action1 = UIAlertAction(title: "Cancel", style: .cancel) { alert in
                
            }
            
            alert.addAction(action)
            alert.addAction(action1)
            present(alert, animated: true, completion: nil)
            
        }
        self.view.makeToast(msg)

    }
    
    //MARK: - API
    
    func MakeAPICallforEmailSettingsPage(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_EmailSettingPage, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let site = data["site"] as? [String:Any]{
                                        self.setEmailSettingsDefaultData(site)
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallforUpdateEmailSettings(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_SaveEmailSettings, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }
                        }
                        if let msg = json["message"] as? String{
                            self.view.makeToast(msg)
                        }

                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallforSendTestMail(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_SendTestEmail, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let site = data["site"] as? [String:Any]{
                                        //                                self.setEmailSettingsDefaultData(site)
                                    }
                                }
                            }
                        }
                        
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
