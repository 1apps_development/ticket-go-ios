//
//  TicketFiledVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit
import Alamofire

class TicketFiledVC: UIViewController {
    @IBOutlet weak var tableTicket: UITableView!
    
    var arrTicketFields:[[String:Any]] = [[:]]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableTicket.register(UINib(nibName: "TicketFieldCell", bundle: nil), forCellReuseIdentifier: "TicketFieldCell")
        tableTicket.tableFooterView = UIView()
        refreshList()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshList), name: Notification.Name("REFRESH_FILED"), object: nil)
    }
    
    @objc func refreshList(){
        MakeAPICallforTicketSettingsList(["user_id":getID()])

    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickNewField(_ sender: Any){
        let vc = AddNewFieldVC(nibName: "AddNewFieldVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc func onClickDeleteField(_ sender: UIButton){
        
        if IS_DEMO_MODE == true {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
        }
        else {
            let dic = self.arrTicketFields[sender.tag]
            let id = dic["id"] as! Int
            self.MakeAPICallforDeleteField(["id":id])
        }
    }
    
    //MARK: - API
   
    func MakeAPICallforTicketSettingsList(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_TicketSettinglist, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let customFields = data["custom_field"] as? [[String:Any]]{
                                        self.arrTicketFields = customFields
                                        self.tableTicket.reloadData()
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallforDeleteField(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_DeleteCustomField, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    self.refreshList()
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
}

extension TicketFiledVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTicketFields.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketFieldCell", for: indexPath) as! TicketFieldCell
        cell.selectionStyle = .none
        cell.configureCell(self.arrTicketFields[indexPath.row])
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(onClickDeleteField(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let changeAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // changeAction code
            let vc = AddNewFieldVC(nibName: "AddNewFieldVC", bundle: nil)
            vc.isUpdating = true
            vc.editingDict = self!.arrTicketFields[indexPath.row]
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self!.navigationController?.present(vc, animated: true)

        }

        // Set the button's images
        changeAction.image = UIImage.init(named: "ic_adjust")
        changeAction.title = "Edit"
        changeAction.backgroundColor = hexStringToUIColor(hex: "162C4E")

        return UISwipeActionsConfiguration.init(actions: [changeAction])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
