//
//  AddNewFieldVC.swift
//  Ticket-GO
//
//  Created by mac on 04/04/22.
//

import UIKit
import Alamofire
import iOSDropDown

class AddNewFieldVC: UIViewController {
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblNewField: UILabel!
    @IBOutlet weak var lblCreateBtn: UILabel!
    @IBOutlet weak var txtLblName: UITextField!
    @IBOutlet weak var txtPlaceholdeR: UITextField!
    //    @IBOutlet weak var txtType: DropDown!
    //    @IBOutlet weak var txtRequire: DropDown!
    
    var isUpdating:Bool = false
    var editingDict: [String:Any]!
    var selectedCatIndex: Int = -1
    var arrType = ["Text","Email","Number","Date","Text area"]
    var arrReuire = ["Yes","No"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if self.isUpdating{
            self.setEditingData()
        }
        // Do any additional setup after loading the view.
    }
    
    //    @IBAction func txt_require(_ sender: Any) {
    //        txtRequire.optionArray = arrReuire
    //        txtRequire.selectedRowColor = .lightGray
    //        txtRequire.isSearchEnable = false
    //        txtRequire.resignFirstResponder()
    //        txtRequire.didSelect(completion: { selected, _, _ in
    //            self.txtRequire.text = selected
    //        })
    //    }
    
    //    @IBAction func txt_type(_ sender: Any) {
    //        txtType.resignFirstResponder()
    //        txtType.optionArray = arrType
    //        txtType.selectedRowColor = .lightGray
    //        txtType.isSearchEnable = false
    //        txtType.didSelect(completion: { selected, _, _ in
    //            self.txtType.text = selected
    //        })
    //    }
    func setEditingData(){
        lblCreate.text = "Edit"
        lblNewField.text = "Custom Field"
        lblCreateBtn.text = "Update"
        txtLblName.text = editingDict["name"] as? String
        txtPlaceholdeR.text = editingDict["placeholder"] as? String
    }
    
    //MARK: - IBAction
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any){
        var msg: String = ""
        if txtLblName.text! == ""{
            msg = "Label Name Field Should not Empty."
        }
        else if txtPlaceholdeR.text! == ""{
            msg = "Placeholder Field Should not Empty."
        }
        //        else if txtType.text! == "" {
        //            msg = "Type Field Should not Empty."
        //        }
        //        else if txtRequire.text! == "" {
        //            msg = "Required Field Should not Empty."
        //        }
        else{
            var params:[String:Any] = [:]
            if isUpdating{
                let id = editingDict["id"] as? Int
                params = ["id":id!,
                          "user_id":getID(),
                          "name":txtLblName.text!,
                          "placeholder":txtPlaceholdeR.text!]
            }
            else{
                params = ["user_id":getID(),
                          "name":txtLblName.text!,
                          "placeholder":txtPlaceholdeR.text!]
                
            }
            self.MakeAPICallforCreateField(params)
        }
    }
    
    //MARK: - API
    
    func MakeAPICallforCreateField(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_CreateCustomField, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            } else if status == 1 {
                                if let msg = json["message"] as? String{
                                    self.view.makeToast(msg)
                                    let notification = Notification.init(name: Notification.Name("REFRESH_FILED"), object: nil, userInfo: nil)
                                    NotificationCenter.default.post(notification)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                        
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
