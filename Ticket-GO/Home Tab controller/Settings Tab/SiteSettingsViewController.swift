//
//  SiteSettingsViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit
import Alamofire
import iOSDropDown

class SiteSettingsViewController: UIViewController {
    @IBOutlet weak var viewAddAttachment: UIView!
    @IBOutlet weak var viewLogo1: UIView!
    @IBOutlet weak var viewLogo2: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var btnGdpr: UIButton!
    @IBOutlet weak var imgFavicon: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgWhiteLogo: UIImageView!
    @IBOutlet weak var txtAppname: UITextField!
    @IBOutlet weak var txtLanguage: DropDown!
    @IBOutlet weak var txtFooter: UITextField!
    @IBOutlet weak var txtGdprCookie: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        MakeAPICallforLanguageList()
        txtLanguage.selectedRowColor = .lightGray
        txtLanguage.isSearchEnable = false
        txtLanguage.arrowSize = 0
        txtLanguage.checkMarkEnabled = false
        txtLanguage.didSelect(completion: { selected, Index, _ in
            //            self.selectedCatIndex = Index
            self.txtLanguage.text = selected
        })
        
        viewAddAttachment.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        viewLogo1.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        viewLogo2.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        btnLang.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnLang.setImage(UIImage.init(named: "switch_off"), for: .normal)
        btnGdpr.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnGdpr.setImage(UIImage.init(named: "switch_off"), for: .normal)
        self.MakeAPICallforSiteSettingsPage(["id":getID()])
    }
    
    func setSiteSettingsDefaultData(_ dict:[String:Any])
    {
        if let appname = dict["name"] as? String{
            self.txtAppname.text = appname
        }
        if let siteRtl = dict["SITE_RTL"] as? String{
            self.btnLang.isSelected = siteRtl == "off" ? false : true
        }
        if let gdpr = dict["gdpr_cookie"] as? String{
            self.btnGdpr.isSelected = gdpr == "off" ? false : true
        }
        if let cookietext = dict["cookie_text"] as? String{
            self.txtGdprCookie.text = cookietext
        }
        if let footer = dict["FOOTER_TEXT"] as? String{
            self.txtFooter.text = footer
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCLickRTL(_ sender: Any){
        btnLang.isSelected = btnLang.isSelected == false ? true : false
    }
    @IBAction func onClickGDPR(_ sender:Any){
        btnGdpr.isSelected = btnGdpr.isSelected == false ? true : false
    }
    @IBAction func onClickFavicon(_ sender: Any){
        ImagePickerManager().pickImage(self){ image in
            self.imgFavicon.image = image
        }
    }
    @IBAction func onClickLogo(_ sender: Any){
        ImagePickerManager().pickImage(self){ image in
            
            //here is the image
            self.imgLogo.image = image
        }
    }
    @IBAction func onClickWhiteLogo(_ sender: Any){
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.imgWhiteLogo.image = image
        }
    }
    @IBAction func onClickSaveChanges(_ sender: Any){
        var msg: String = ""
        if imgFavicon.image == nil{
            msg = "Please select Image for favicon"
        }else if imgLogo.image == nil{
            msg = "Please select Image for Logo"
        }else if imgWhiteLogo.image == nil{
            msg = "Please select Image for White Logo"
        }else if imgLogo.image == nil{
            msg = "Please select Image for Logo"
        }else if txtAppname.text! == ""{
            msg = "App Name Field should not empty"
        }else if txtLanguage.text! == ""{
            msg = "Language Field should not empty"
        }else if txtFooter.text! == ""{
            msg = "Footer Field should not empty"
        }else if txtGdprCookie.text! == ""{
            msg = "Cookie Field should not empty"
        }else{
            let param:[String:String] = ["id":"\(getID())",
                                         "app_name":txtAppname.text!,
                                         "default_language":txtLanguage.text!,
                                         "site_rtl":btnLang.isSelected == true ? "on" : "off",
                                         "gdpr_cookie":btnGdpr.isSelected == true ? "on" : "off",
                                         "cookie_text":txtGdprCookie.text!,
                                         "footer_text":txtFooter.text!]
            self.MakeAPICallforUpdateSiteSettings(param)
        }
        
        self.view.makeToast(msg)
        
        
    }
    
    //MARK: - API
    
    func MakeAPICallforSiteSettingsPage(_ param:[String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_SiteSettingsPage, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let site = data["site"] as? [String:Any]{
                                        self.setSiteSettingsDefaultData(site)
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPICallforLanguageList(){
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        
        ServiceManager.callGetApi(URL_LanguageList, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                        }
                        if let languages = data["language"] as? [String]{
                            self.txtLanguage.optionArray = languages
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforUpdateSiteSettings(_ param:[String:String])
    {
        SHOW_CUSTOM_LOADER()
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param{
                multipartFormData.append((value).data(using: .utf8)!, withName: key)
            }
            if let jpegData = self.imgFavicon.image!.jpegData(compressionQuality: 0.4) {
                multipartFormData.append(jpegData, withName: "favicon", fileName: "image_\(randomString(length: 4)).png", mimeType: "image/png")
            }
            
            if let jpegData = self.imgLogo.image!.jpegData(compressionQuality: 0.4) {
                multipartFormData.append(jpegData, withName: "logo", fileName: "image_\(randomString(length: 4)).png", mimeType: "image/png")
            }
            
            if let jpegData = self.imgWhiteLogo.image!.jpegData(compressionQuality: 0.4) {
                multipartFormData.append(jpegData, withName: "white_logo", fileName: "image_\(randomString(length: 4)).png", mimeType: "image/png")
            }
        },
                  to: URL_UpdateSiteSettings, headers: headers).response(completionHandler: { response in
            switch response.result{
            case .success(_):
                HIDE_CUSTOM_LOADER()
                
                if response.response?.statusCode == 200 {
                    print("OK. Done")
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: response.data!, options : .allowFragments) as? Dictionary<String,Any>
                        {
                            print(jsonArray) // use the json here
                            if let msg = jsonArray["message"] as? String{
                                self.view.makeToast(msg, point: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2), title: "Settings Saved Successfully!", image: nil) { didTap in
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            //                            if let data = jsonArray["data"] as? [String:Any]{
                            //
                            //                            }
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }else if response.response?.statusCode == 406{
                    print("error")
                    DispatchQueue.main.sync{
                        let alert = UIAlertController(title: "Error", message:
                                                        "Person has not been set up.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "close", style: .default, handler: { action in
                            DispatchQueue.main.async{
                                HIDE_CUSTOM_LOADER()
                                
                                //                                self.progressUiView.isHidden = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }))
                        
                        self.present(alert, animated: true)
                    }
                }else{
                    print(response.response?.statusCode)
                }
                
                
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        })
    }
    
}
