//
//  DashboardVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import JJFloatingActionButton
import Alamofire
import SDWebImage
import AAInfographics


class DashboardVC: UIViewController, JJFloatingActionButtonDelegate {
    
    func floatingActionButtonWillOpen(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = .white
        viewCircle.isHidden = false
    }
    func floatingActionButtonWillClose(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        viewCircle.isHidden = true
    }
    
    @IBOutlet weak var segmentTodays: HBSegmentedControl!
    @IBOutlet weak var segmentTask: HBSegmentedControl!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var tableTickets: UITableView!
    //    @IBOutlet weak var tableMoreTickets: UITableView!
    @IBOutlet weak var tableAnlyCategories: UITableView!
    @IBOutlet var heightTableAnlyCat: NSLayoutConstraint!
    @IBOutlet weak var lblBaseUrl: UILabel!
    @IBOutlet weak var topCompanyLogo: UIImageView!
    @IBOutlet weak var img_User: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblGreetings: UILabel!
    @IBOutlet weak var lblNewTicketsmsg: UILabel!
    @IBOutlet weak var lblNewTicketsCount: UILabel!
    @IBOutlet weak var viewNewTicketsMsg: UIView!
    @IBOutlet weak var lblTotalCategories: UILabel!
    @IBOutlet weak var lblOpenTickets: UILabel!
    @IBOutlet weak var lblTotalClosedTickets: UILabel!
    @IBOutlet weak var lblTotalAgents: UILabel!
    @IBOutlet weak var lblCat_center_per: UILabel!
    @IBOutlet weak var lblTick_center_per: UILabel!
    @IBOutlet weak var lbl_center_name_cat: UILabel!
    @IBOutlet weak var lbl_center_name_tick: UILabel!
    @IBOutlet weak var lbl_date_corner: UILabel!
    @IBOutlet weak var cat_circle_1: KDCircularProgress!
    @IBOutlet weak var cat_circle_2: KDCircularProgress!
    @IBOutlet weak var cat_circle_3: KDCircularProgress!
    @IBOutlet weak var tick_circle_1: KDCircularProgress!
    @IBOutlet weak var tick_circle_2: KDCircularProgress!
    @IBOutlet weak var tick_circle_3: KDCircularProgress!
    @IBOutlet weak var lblNewTickets_: UILabel!
    @IBOutlet weak var lblOpenTickets_: UILabel!
    @IBOutlet weak var lblClosedTickets_: UILabel!
    @IBOutlet weak var viewChartContainer: UIView!
    @IBOutlet var height_table_tickets: NSLayoutConstraint!
    
    var arrLastTickets:[TicketsModel] = []
    var arrCategories : [categoryMoreModel] = []
    
    public var aaChartModel: AAChartModel!
    public var aaChartView: AAChartView!
    
    
    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.MakeAPICallforHomeData(["id":getID()])
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        
        self.MakeAPICallforHomeData(["id":getID()])
        //        setUpAAGraphView()
        self.lbl_date_corner.text = getCurrentDate()
        
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        actionButton.delegate = self
        actionButton.buttonImageColor = hexStringToUIColor(hex: "172C4E")
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonImageSize = CGSize(width: 16, height: 16)
        actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: 80)
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        actionButton.buttonAnimationConfiguration.opening.duration = 0.8
        actionButton.buttonAnimationConfiguration.closing.duration = 0.6
        for item in actionButton.items{
            item.imageView.tintColor = hexStringToUIColor(hex: "172C4E")
        }
        if #available(iOS 13.0, *) {
            actionButton.buttonImage = UIImage.init(named: "ic_plus")?.withTintColor(hexStringToUIColor(hex: "172C4E"))
        } else {
            // Fallback on earlier versions
        }
        let imgeTickets = UIImage.init(named: "tab_tickets")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imgageCateg = UIImage.init(named: "tab_categories")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageUser = UIImage.init(named: "User")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        
        actionButton.addItem(image: imgeTickets) { item in
            let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = .clear
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        actionButton.addItem(image: imgageCateg) { item in
            //            Helper.showAlert(for: item)
            let vc = CreateNewCategoryVC(nibName: "CreateNewCategoryVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        
        actionButton.addItem(image: imageUser) { item in
            //            Helper.showAlert(for: item)
            let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true)
            
        }
        
        //        actionButton.display(inViewController: self)
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -18).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -36).isActive = true
        viewCircle.center = actionButton.center
        viewCircle.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        //money
        segmentTodays.items = ["Today", "Week", "Month"]
        segmentTodays.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTodays.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTodays.selectedLabelColor = .white
        segmentTodays.backgroundColor = .clear
        segmentTodays.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTodays.selectedIndex = 0
        
        //invoice
        segmentTask.items = ["Today", "Week", "Month"]
        segmentTask.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTask.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTask.selectedLabelColor = .white
        segmentTask.backgroundColor = .clear
        segmentTask.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTask.selectedIndex = 0
        
        // Do any additional setup after loading the view.
        
        let nib = UINib(nibName: "TicketHeader", bundle: nil)
        let cellnib = UINib(nibName: "TicketsListCell", bundle: nil)
        
        tableTickets.register(nib, forHeaderFooterViewReuseIdentifier: "TicketHeader")
        tableTickets.register(cellnib, forCellReuseIdentifier: "TicketsListCell")
        tableTickets.tableFooterView = UIView()
        tableAnlyCategories.register(UINib(nibName: "CategoryMoreCell", bundle: nil), forCellReuseIdentifier: "CategoryMoreCell")
        tableAnlyCategories.tableFooterView = UIView()
        //        tableMoreTickets.register(UINib.init(nibName: "TicketMoreCell", bundle: nil), forCellReuseIdentifier: "TicketMoreCell")
        //        tableMoreTickets.tableFooterView = UIView()
        
    }
    
    func setUpAAGraphView(_ dict:[String:Any] = [:]) {
        aaChartView = AAChartView()
        var arrSeries:[Any] = []
        let chartViewWidth = viewChartContainer.frame.size.width
        let chartViewHeight = viewChartContainer.frame.size.height
        aaChartView!.frame = CGRect(x: 0,
                                    y: 0,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        viewChartContainer.addSubview(aaChartView!)
        aaChartView!.isScrollEnabled = true//Disable chart content scrolling
        aaChartView!.isClearBackgroundColor = true
        aaChartView!.delegate = self as AAChartViewDelegate
        var xData:[String] = ["15.Aug", "16.Aug", "17.Aug", "18.Aug", "19.Aug", "20.Aug","21.Aug", "22.Aug"]
        if let xAxisdata = dict["x_axis"] as? [String]{
            xData = xAxisdata
        }
        var dColors:[String] = ["#89E065","#FC275A"]
        
        if let yAxdata = dict["y_axis"] as? [[String:Any]]{
            dColors = []
            for i in 0..<yAxdata.count{
                let dic = yAxdata[i]
                if let color = dic["color"] as? String{
                    dColors.append(color)
                }
                let name = dic["name"] as! String
                let data = dic["data"] as! [Any]
                var aaSeries: AASeriesElement!
                aaSeries = AASeriesElement()
                aaSeries.name(name)
                aaSeries.data(data)
                arrSeries.append(aaSeries!)
            }
        }
        
        aaChartModel = AAChartModel()
            .chartType(AAChartType.spline)
            .colorsTheme(dColors)//Colors theme
            .xAxisLabelsStyle(AAStyle(color: AAColor.rgbaColor(23, 44, 78)))
            .dataLabelsEnabled(false)
        //            .tooltipValueSuffix("℃")
            .markerSymbolStyle(.borderBlank)//Set the polyline connection point style to: white edge
            .categories(xData)
            .animationType(.swingFromTo)
            .series(arrSeries)
        
        aaChartModel!.markerSymbol = .circle
        aaChartModel!.markerRadius(0)
        aaChartView!.aa_drawChartWithChartModel(aaChartModel!)
    }
    
    func setDataOfChart(_ dict: NSDictionary){
        let newTicketPer = dict["new_ticket"] as! Int
        let openTicketPer = dict["open_ticket"] as! Int
        let closedTicketPer = dict["close_ticket"] as! Int
        
        self.lblNewTickets_.text = String(format: "%.2d %%", newTicketPer)
        self.lblTick_center_per.text = String(format: "%.2d %%", newTicketPer)
        self.lblOpenTickets_.text = String(format: "%.2d %%", openTicketPer)
        self.lblClosedTickets_.text = String(format: "%.2d %%", closedTicketPer)
        
        self.tick_circle_1.progress = Double(newTicketPer) / 100
        self.tick_circle_2.progress = Double(openTicketPer) / 100
        self.tick_circle_3.progress = Double(closedTicketPer) / 100
        
    }
    
    func setProgressData()
    {
        if arrCategories.count > 2{
            let firstObject = arrCategories[0].value
            let secondObject = arrCategories[1].value
            let thirdObject = arrCategories[2].value
            
            self.lblCat_center_per.text = String(format: "%.2d %%", firstObject!)
            self.lbl_center_name_cat.text = arrCategories[0].category!
            
            self.cat_circle_1.progress = Double(firstObject!) / 100
            self.cat_circle_2.progress = Double(secondObject!) / 100
            self.cat_circle_3.progress = Double(thirdObject!) / 100
            
            self.cat_circle_1.progressColors = [hexStringToUIColor(hex: arrCategories[0].color!)]
            self.cat_circle_2.progressColors = [hexStringToUIColor(hex: arrCategories[1].color!)]
            self.cat_circle_3.progressColors = [hexStringToUIColor(hex: arrCategories[2].color!)]
        }
    }
    
    
    func setUserdata(_ dict: NSDictionary){
        self.lblBaseUrl.text = URL_BASE
        if let imgStr = dict["image_url"] as? String{
            if let imgUrl = URL(string: imgStr){
                self.img_User.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
            }
            
            if let name = dict["name"] as? String{
                self.lblUsername.text = "Morning,\n\(name)!"
            }
            let ticketcount = dict["total_ticket"] as! Int
            self.lblNewTicketsCount.text = "\(ticketcount)"
            self.lblNewTicketsmsg.text = "You have new \(ticketcount) Tickets"
            self.lblGreetings.text = dict["email"] as? String
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Dashboard"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        
        self.navigationController?.present(vc, animated: true)
    }
    
    @IBAction func onClickEditProfile(_ sender: Any){
        let vc = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - API
    
    func MakeAPICallforHomeData(_ params: [String:Any])
    {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers : HTTPHeaders = ["Authorization": "Bearer \(token)",
                                         "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Home, params: params, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as?  Int{
                            if status == 401{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    appDelegate.logoutWithoutAPI()
                                }
                                break
                            }else if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let userData = data["user_data"] as? [String:Any]{
                                        self.setUserdata(userData as NSDictionary)
                                    }
                                    if let statistics = data["statistics"] as? [String:Any]{
                                        let category = statistics["category"] as! Int
                                        let openTicket = statistics["open_ticket"] as! Int
                                        let closeTicket = statistics["close_ticket"] as! Int
                                        let agents = statistics["agents"] as! Int
                                        self.lblTotalCategories.text = "\(category)"
                                        self.lblTotalClosedTickets.text = "\(closeTicket)"
                                        self.lblTotalAgents.text = "\(agents)"
                                        self.lblOpenTickets.text = "\(openTicket)"
                                    }
                                    if let tickets = data["last_ticket"] as? [[String:Any]]{
                                        self.arrLastTickets = TicketsSuperModel.init(tickets).tickets
                                        self.height_table_tickets.constant = CGFloat(self.arrLastTickets.count * 117)
                                        self.view.layoutIfNeeded()
                                        self.tableTickets.reloadData()
                                    }
                                    if let categoryAnalysis = data["category_analytics"] as? [[String:Any]]{
                                        self.arrCategories = categoriesSuperModel.init(categoryAnalysis).categoriesMore
                                        self.setProgressData()
                                        self.tableAnlyCategories.reloadData()
                                        self.heightTableAnlyCat.constant = CGFloat(self.arrCategories.count * 60)
                                        self.view.layoutIfNeeded()
                                    }
                                    if let ticketData = data["ticket_analytics"] as? [String:Any]{
                                        self.setDataOfChart(ticketData as NSDictionary)
                                    }
                                    if let graphData = data["graph_data"] as? [String:Any]{
                                        self.setUpAAGraphView(graphData)
                                    }
                                }
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallforDeleteTicket(_ param:[String:Int]) {
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(getToken())"]
        ServiceManager.callPostApi(URL_DeleteTicket, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String
                        {
                            self.view.makeToast(msg)
                            self.MakeAPICallforHomeData(["id":getID()])
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
}

extension DashboardVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableAnlyCategories{
            return arrCategories.count
        }
        return arrLastTickets.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTickets{
            let cell: TicketsListCell = tableView.dequeueReusableCell(withIdentifier: "TicketsListCell") as! TicketsListCell
            cell.configureCell(arrLastTickets[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryMoreCell") as! CategoryMoreCell
        cell.selectionStyle = .none
        cell.configureCell(arrCategories[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableTickets{
            return 117
        }
        return 60
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == self.tableTickets{
            
            
            let replyAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
                // changeAction code
                let vc = ReplyTicketVC(nibName: "ReplyTicketVC", bundle: nil)
                vc.modalPresentationStyle = .overCurrentContext
                
                vc.ticket_id = self!.arrLastTickets[indexPath.row].id!
                self!.navigationController?.present(vc, animated: true, completion:  nil)
                
            }
            replyAction.image = UIImage.init(named: "ic_reply")
            replyAction.title = "Reply"
            replyAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
            
            let deleteaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
                success(true)
                
                if IS_DEMO_MODE == false {
                    let category = self.arrLastTickets[indexPath.row]
                    self.tableTickets.reloadData()
                    self.MakeAPICallforDeleteTicket(["id":category.id!])
                    self.MakeAPICallforHomeData(["id":getID()])
                }
                else {
                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
                }
            }
            deleteaction.image = UIImage.init(named: "ic_delete")
            deleteaction.title = "Delete"
            deleteaction.backgroundColor = hexStringToUIColor(hex: "FC275A")
            
            return UISwipeActionsConfiguration.init(actions: [deleteaction,replyAction])
        }
        return nil
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableTickets {
            var ticketModl = TicketsModel()
            ticketModl = arrLastTickets[indexPath.row]
            
            let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
            vc.isUpdating = true
            vc.ticketInfoToUpdate = ticketModl
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = .clear
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}

extension DashboardVC: AAChartViewDelegate {
    open func aaChartViewDidFinishLoad(_ aaChartView: AAChartView) {
        print("🚀🚀🚀, AAChartView Did Finished Load!!!")
    }
    
    open func aaChartView(_ aaChartView: AAChartView, clickEventMessage: AAClickEventMessageModel) {
        print(
            """
            
            clicked point series element name: \(clickEventMessage.name ?? "")
            🖱🖱🖱WARNING!!!!!!!!!!!!!!!!!!!! Click Event Message !!!!!!!!!!!!!!!!!!!! WARNING🖱🖱🖱
            ==========================================================================================
            ------------------------------------------------------------------------------------------
            user finger CLICKED!!!,get the custom click event message: {
            category = \(String(describing: clickEventMessage.category))
            index = \(String(describing: clickEventMessage.index))
            name = \(String(describing: clickEventMessage.name))
            offset = \(String(describing: clickEventMessage.offset))
            x = \(String(describing: clickEventMessage.x))
            y = \(String(describing: clickEventMessage.y))
            }
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            
            """
        )
    }
    
    open func aaChartView(_ aaChartView: AAChartView, moveOverEventMessage: AAMoveOverEventMessageModel) {
        print(
            """
            
            moved over point series element name: \(moveOverEventMessage.name ?? "")
            ✋🏻✋🏻✋🏻✋🏻✋🏻WARNING!!!!!!!!!!!!!! Move Over Event Message !!!!!!!!!!!!!! WARNING✋🏻✋🏻✋🏻✋🏻✋🏻
            ==========================================================================================
            ------------------------------------------------------------------------------------------
            user finger MOVED OVER!!!,get the move over event message: {
            category = \(String(describing: moveOverEventMessage.category))
            index = \(String(describing: moveOverEventMessage.index))
            name = \(String(describing: moveOverEventMessage.name))
            offset = \(String(describing: moveOverEventMessage.offset))
            x = \(String(describing: moveOverEventMessage.x))
            y = \(String(describing: moveOverEventMessage.y))
            }
            +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            
            """
        )
    }
}
