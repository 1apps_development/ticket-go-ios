//
//  Models.swift
//  Ticket-GO
//
//  Created by mac on 25/07/22.
//

import Foundation
import UIKit

class TicketsSuperModel: NSObject{
    var tickets:[TicketsModel] = []
    init(_ ticketArr:[[String:Any]]){
        for i in 0..<ticketArr.count{
            let dic = ticketArr[i]
            tickets.append(TicketsModel.init(dic))
        }
    }
}

class TicketsModel: NSObject{
    static var SharedInstance = TicketsModel()
    override init() {
        super.init()
    }

    var id: Int? = 0
    var ticketId: String? = ""
    var name: String? = ""
    var email: String? = ""
    var category: String? = ""
    var color: String? = ""
    var subject: String? = ""
    var status: String? = ""
    var desc: String? = ""
    var attachments: [Any] = []
    var note: String? = ""
    var times: String? = ""
    
    init(_ dict: [String:Any]){
        id = dict["id"] as? Int
        ticketId = dict["ticket_id"] as? String
        name = dict["name"] as? String
        email = dict["email"] as? String
        category = dict["category"] as? String
        color = dict["color"] as? String
        subject = dict["subject"] as? String
        status = dict["status"] as? String
        desc = dict["description"] as? String
        if let attach = dict["attachments"] as? [[String:Any]]{
            attachments = attach
        }
        note = dict["note"] as? String
        times = dict["time"] as? String
    }
}

class customFieldsSuperModel : NSObject
{
    var customFields:[customFieldModel] = []
    init(_ dataArr:[[String:Any]]){
        for i in 0..<dataArr.count{
            customFields.append(customFieldModel.init(dataArr[i]))
        }
    }
}

class customFieldModel: NSObject{
    var id: Int? = 0
    var name: String? = ""
    var type: String? = ""
    var placeHolder: String? = ""
    init(_ dict:[String:Any])
    {
        id = dict["id"] as? Int
        name = dict["name"] as? String
        type = dict["type"] as? String
        placeHolder = dict["plalceholder"] as? String
    }
}

class categoriesSuperModel: NSObject{
    var categories:[categoryModel] = []
    var categoriesMore :[categoryMoreModel] = []
    init(_ catArr:[[String:Any]])
    {
        for i in 0..<catArr.count{
            categories.append(categoryModel.init(catArr[i]))
        }
        for i in 0..<catArr.count{
            categoriesMore.append(categoryMoreModel.init(catArr[i]))
        }
    }
    
}

class categoryModel: NSObject{
    var id: Int? = 0
    var name : String? = ""
    var color : String? = ""
        
    init(_ dict:[String:Any]){
        id = dict["id"] as? Int
        name = dict["name"] as? String
        color = dict["color"] as? String
    }
}

class categoryMoreModel: NSObject{
    var category: String? = ""
    var color: String? = ""
    var value: Int? = 0
    
    init(_ dict:[String:Any])
    {
        category = dict["category"] as? String
        color = dict["color"] as? String
        value = dict["value"] as? Int
    }
}

class usersSuperModel: NSObject{
    var users:[userModel] = []
    init(_ userArr:[[String:Any]])
    {
        for i in 0..<userArr.count
        {
            users.append(userModel.init(userArr[i]))
        }
                
    }
}
class userModel: NSObject{
    static var SharedInstance = userModel()
    override init() {
        super.init()
    }

    var id: Int? = 0
    var name: String? = ""
    var email: String? = ""
    var avtar: String? = ""
    var role: String? = ""
    init(_ dict:[String:Any])
    {
        id = dict["id"] as? Int
        name = dict["name"] as? String
        email = dict["email"] as? String
        avtar = dict["avatarlink"] as? String
        role = dict["role"] as? String
    }
}
//no in use
class userInfoModel: NSObject{
    /*
     "id": 39,
                 "name": "The Tester",
                 "email": "thetest@yopmail.com",
                 "email_verified_at": null,
                 "created_at": "2022-08-09T09:04:29.000000Z",
                 "updated_at": "2022-08-09T09:04:29.000000Z",
                 "avatar": "avatar-1660035869.jpg",
                 "parent": 1,
                 "lang": "en",
                 "device_type": null,
                 "token": null,
                 "avatarlink": "https://apps.rajodiya.com/ticketgo/storage//public/avatar-1660035869.jpg"
     */
    var id: Int? = 0
    var name: String? = ""
    var email: String? = ""
}

class rolesSuperModel: NSObject{
    var roles:[rolesModel] = []
    init(_ roleArr:[[String:Any]]){
        for i in 0..<roleArr.count{
            roles.append(rolesModel.init(roleArr[i]))
        }
    }
}

class rolesModel: NSObject{
   
    var id: Int? = 0
    var name: String? = ""
    var guardName: String? = ""
    
    init(_ dict:[String:Any])
    {
        id = dict["id"] as? Int
        name = dict["name"] as? String
        guardName = dict["guard_name"] as? String
    }
}

class faqSuperModel: NSObject{
    var faqs : [faqModel] = []
    init(_ faqArr:[[String:Any]]){
        for i in 0..<faqArr.count{
            faqs.append(faqModel.init(faqArr[i]))
        }
    }
}

class faqModel: NSObject{
    var id: Int? = 0
    var title: String? = ""
    var descreption: String? = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
    
    init(_ dict:[String:Any]){
        id = dict["id"] as? Int
        title = dict["title"] as? String
        descreption = dict["description"] as? String
        createdAt = dict["created_at"] as? String
        updatedAt = dict["updated_at"] as? String
    }
}
