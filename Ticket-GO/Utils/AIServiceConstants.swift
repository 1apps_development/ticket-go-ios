//
//  AIServiceConstants.swift
//
//  Created by Vrushik on 08/02/2021.
//  Copyright © 2021 Vrushik. All rights reserved.
//
import Foundation

//MARK:- BASE URL

//Live URL
//let URL_BASE                              = "https://apps.rajodiya.com/ticketgo"
var URL_BASE                              = "https://apps.rajodiya.com/ticketgo_saas"


//Test URL
//let URL_BASE                            = "http://192.168.0.200:4932/"
//let URL_Local_Server                    = "http://192.168.0.114:8000/"

let URL_Login                           = getFullUrl("/api/login")
let URL_Signup                          = getFullUrl("/api/login")
let URL_Logout                          = getFullUrl("/api/logout")
let URL_Home                            = getFullUrl("/api/home")
let URL_TicketPage                      = getFullUrl("/api/ticket")
let URL_DeleteTicket                    = getFullUrl("/api/ticket_delete")
let URL_CreateTicket                    = getFullUrl("/api/ticket_create")
let URL_OpenTicket                      = getFullUrl("/api/openticket")
let URL_ReplayTicket                    = getFullUrl("/api/replayticket")
let URL_GetCustomFields                 = getFullUrl("/api/getcoustomfield")
let URL_GetCategories                   = getFullUrl("/api/category")
let URL_Categories                      = getFullUrl("/api/getcategory")
let URL_CreateCategory                  = getFullUrl("/api/create_category")
let URL_DeleteCategory                  = getFullUrl("/api/delete_category")
let URL_GetUsersList                    = getFullUrl("/api/users")
let URL_DeleteUser                      = getFullUrl("/api/user_delete")
let URL_CreateUser                      = getFullUrl("/api/user_create")
let URL_GetRole                         = getFullUrl("/api/role")
let URL_GetUser                         = getFullUrl("/api/getuser")
let URL_FAQList                         = getFullUrl("/api/faq")
let URL_DeleteFAQ                       = getFullUrl("/api/faq_delete")
let URL_CreateFAQ                       = getFullUrl("/api/faq_create")
let URL_SiteSettingsPage                = getFullUrl("/api/site_setting_page")
let URL_UpdateSiteSettings              = getFullUrl("/api/sitesetting")
let URL_EmailSettingPage                = getFullUrl("/api/emailsettingpage")
let URL_SaveEmailSettings               = getFullUrl("/api/emailsetting")
let URL_SendTestEmail                   = getFullUrl("/api/test_email_send")
let URL_RecaptchaSettings               = getFullUrl("/api/recaptchasetting")
let URL_LanguageList                    = getFullUrl("/api/lang")
let URL_TicketSettinglist               = getFullUrl("/api/ticketsetting")
let URL_CreateCustomField               = getFullUrl("/api/coustomfield")
let URL_DeleteCustomField               = getFullUrl("/api/deletecoustomfield")
let URL_ReplyTicket                     = getFullUrl("/api/replayticket")
    
//MARK:- FULL URL

func getFullUrl(_ urlEndPoint : String) -> String {
    return URL_BASE + urlEndPoint
}


extension String
{

static let str_please_enable_camera_permission_in_setting = "Please enable camera Permission in Settings > Ticket-Go > enable camera permission"
    static let str_settings = "Settings"
}
