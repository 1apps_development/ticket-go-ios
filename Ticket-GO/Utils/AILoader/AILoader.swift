//
//  AILoader.swift
//
//  Created by Vrushik on 13/05/17.
//  Copyright © 2016 Vrushik. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
//import Lottie


private var activityRestorationIdentifier: String {
    return "NVActivityIndicatorViewContainer"
}

public func ShowLoaderWithMessage(message:String) {
    startActivityAnimating(size: CGSize(width:56, height:56), message: message, type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.white, padding: 2,isFromOnView: false)
}

//MARK: - ShowJsonLoader
public func SHOW_CUSTOM_LOADER_LOTTIE(){
//    startActivityJson(isFromOnView: false)
}


//MARK:- ShowLoader
//Mark:-


public func SHOW_CUSTOM_LOADER() {
    startActivityAnimating(size: CGSize(width:56, height:56), message: nil, type: NVActivityIndicatorType.circleStrokeSpin, color: APP_PRIMARY_DEEP_BLUE_COLOR, padding: 2,isFromOnView: false)
}


//MARK:- Hide Loader
//MARK:-


public func HIDE_CUSTOM_LOADER() {
    stopActivityAnimating(isFromOnView: false)
}


//MARK:- ShowLoaderOnView
//Mark:-


public func ShowLoaderOnView() {
    startActivityAnimating(size: CGSize(width:56, height:56), message: nil, type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.white, padding: 2,isFromOnView: true)
}


//MARK:- HideLoaderOnView
//MARK:-


public func HideLoaderOnView() {
    stopActivityAnimating(isFromOnView: true)
}

private func startActivityAnimating(size: CGSize? = nil, message: String? = nil, type: NVActivityIndicatorType? = nil, color: UIColor? = nil, padding: CGFloat? = nil, isFromOnView:Bool) {
    DispatchQueue.main.async {
        let activityContainer: UIView = UIView(frame: CGRect(x:0, y:0,width:SCREEN_WIDTH, height:SCREEN_HEIGHT))
        activityContainer.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        activityContainer.restorationIdentifier = activityRestorationIdentifier
        
        activityContainer.isUserInteractionEnabled = false
        let actualSize = size ?? CGSize(width:56,height:56)
        
        let activityIndicatorView = NVActivityIndicatorView(
            frame: CGRect(x:0, y:0, width:actualSize.width, height:actualSize.height),
            type: type!,
            color: color!,
            padding: padding!)
        
        activityIndicatorView.center = activityContainer.center
        activityIndicatorView.startAnimating()
        activityContainer.addSubview(activityIndicatorView)
        
        
        if message != nil {
            let width = activityContainer.frame.size.width / 2
            if let message = message , !message.isEmpty {
                let label = UILabel(frame: CGRect(x:0, y:0,width:width, height:30))
                label.center = CGPoint(
                    x:activityIndicatorView.center.x, y:
                        activityIndicatorView.center.y + actualSize.height)
                label.textAlignment = .center
                label.text = message
                label.font = UIFont.systemFont(ofSize: 16.0)
                label.textColor = activityIndicatorView.color
                activityContainer.addSubview(label)
            }
        }
        UIApplication.shared.keyWindow?.isUserInteractionEnabled = false
        if isFromOnView == true {
            UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(activityContainer)
        }
        else {
            UIApplication.shared.keyWindow?.addSubview(activityContainer)
        }
    }
}

//private func startActivityJson(size: CGSize? = nil, message: String? = nil,color: UIColor? = nil, padding: CGFloat? = nil, isFromOnView:Bool) {
//    let activityContainer: UIView = UIView(frame: CGRect(x:0, y:0,width:SCREEN_WIDTH, height:SCREEN_HEIGHT))
//    activityContainer.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//    activityContainer.restorationIdentifier = activityRestorationIdentifier
//
//    activityContainer.isUserInteractionEnabled = false
//    let actualSize = size ?? CGSize(width:75,height:75)
//
//    let animation = Animation.named("loader")
//    let activityIndicatorView = AnimationView.init(animation: animation)
//    activityIndicatorView.frame = CGRect.init(x: 0, y: 0, width: actualSize.width, height: actualSize.width)
//    activityIndicatorView.center = activityContainer.center
//    activityIndicatorView.play(fromProgress: 0, toProgress: 1, loopMode: .loop, completion: nil)
//    activityContainer.addSubview(activityIndicatorView)
//    /**        let anim = Animation.named("loader")
//
//     animationView.animation = anim
// animationView.frame = CGRect.init(x: 100, y: 120, width: 50, height: 50)
// animationView.backgroundColor = UIColor.white
//     animationView.contentMode = .scaleAspectFit
//     view.addSubview(animationView)
// animationView.play(fromProgress: 0, toProgress: 1, loopMode: .loop, completion: nil)
//*/
//    UIApplication.shared.keyWindow?.isUserInteractionEnabled = false
//    if isFromOnView == true {
//        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(activityContainer)
//    }
//    else {
//        UIApplication.shared.keyWindow?.addSubview(activityContainer)
//    }
//}


/**
 Stop animation and remove from view hierarchy.
 */

private func stopActivityAnimating(isFromOnView:Bool) {
    DispatchQueue.main.async { // Correct
        
        UIApplication.shared.keyWindow?.isUserInteractionEnabled = true
        if isFromOnView == true {
            for item in (UIApplication.shared.keyWindow?.rootViewController?.view.subviews)!
            where item.restorationIdentifier == activityRestorationIdentifier {
                item.removeFromSuperview()
            }
        }
        else {
            for item in (UIApplication.shared.keyWindow?.subviews)!
            where item.restorationIdentifier == activityRestorationIdentifier {
                item.removeFromSuperview()
            }
        }
    }
}
extension UIColor {
  static func hexColour(hexValue:UInt32)->UIColor
    {
      let red = CGFloat((hexValue & 0xFF0000) >> 16) / 255.0
      let green = CGFloat((hexValue & 0xFF00) >> 8) / 255.0
      let blue = CGFloat(hexValue & 0xFF) / 255.0
      return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
