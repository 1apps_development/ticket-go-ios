//
//  LoginViewController.swift
//  Taskly
//
//  Created by Vrushik on 07/03/22.
//

import UIKit
import GoogleSignIn
import AuthenticationServices


class LoginViewController: UIViewController {
    @IBOutlet weak var viewGoogleSignin: UIView!
    let signInConfig = GIDConfiguration(clientID: "338614670794-2cu0eonjgok3nq9425pq2fl4ulml8epc.apps.googleusercontent.com")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBAction
    
    @IBAction func onClickLoginwithEmail(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoginEmailViewController") as! LoginEmailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFacebook(_ sender: Any)
    {
//        self.FacebookLogin()
    }
    @IBAction func onClickGoogle(_ sender: Any)
    {
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            guard let user = user else { return }
            
            let googleId = user.userID!
            //   let givenName : String = profiledata.givenName ?? ""
            // let familyName : String = profiledata.familyName ?? ""
            let name = user.profile?.name
            let email = user.profile?.email

            let emailAddress = user.profile?.email

            let fullName = user.profile?.name
            let givenName = user.profile?.givenName
            let familyName = user.profile?.familyName

            let profilePicUrl = user.profile?.imageURL(withDimension: 320)
   
            GIDSignIn.sharedInstance.disconnect()
            GIDSignIn.sharedInstance.signOut()


//            let urlString = API_URL + "login"
//            let params: NSDictionary = ["email":email!,
//                                        "password":"",
//                                        "device_type":App_device_type,
//                                        "google_id":googleId,
//                                        "facebook_id":"",
//                                        "apple_id":"",
//                                        "token":""]

        }

    }
    @IBAction func onClickApple(_ sender: Any)
    {
        
    }
    @IBAction func onClickSignup(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension LoginViewController
//{

//    func FacebookLogin()
//    {
//        UserDefaults.standard.set(key_facebook, forKey: key_Type)
//        let loginManager = LoginManager()
//        loginManager.logIn(permissions: [.email], viewController: nil) { (loginResult) in
//            switch loginResult {
//            case .success( _, _, _):
//                let dictParamaters = ["fields":"id, name, email"]
//                let request: GraphRequest = GraphRequest.init(graphPath: "me", parameters: dictParamaters)
//                request.start { (connection, result, error) in
//                    let responseData = result as! NSDictionary
//                    let facebookId = responseData["id"] as! String
//                    let name = responseData["name"] as! String
//                    var email = ""
//                    if responseData["email"] != nil {
//                        email = responseData["email"] as! String
//                    }
//                    loginManager.logOut()
//                    let urlString = API_URL + "login"
//                    let params: NSDictionary = ["email":email,
//                                                "password":"",
//                                                "device_type":App_device_type,
//                                                "google_id":"",
//                                                "facebook_id":facebookId,
//                                                "apple_id":"",
//                                                "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken)]
//                    self.Webservice_Login(url: urlString, params: params)
//
//
//
//                }
//                break
//            case .cancelled:
//                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Something went wrong. Try again later")
//                break
//            case .failed( _):
//                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Something went wrong. Try again later")
//                break
//            }
//        }
//    }



//    func AppleLogin(){
//        let request = ASAuthorizationAppleIDProvider().createRequest()
//        request.requestedScopes = [.fullName, .email]
//        let controller = ASAuthorizationController(authorizationRequests: [request])
//        controller.delegate = self
//        controller.presentationContextProvider = self
//        controller.performRequests()
//    }

//}
//extension LoginViewController: ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        switch authorization.credential {
//        case let appleIDCredential as ASAuthorizationAppleIDCredential:
//
//            let userIdentifier = appleIDCredential.user
//            var Email = String()
//            var Fullname = String()
//            if appleIDCredential.email != nil
//            {
//                Email = appleIDCredential.email!
//                UserDefaultManager.setStringToUserDefaults(value: Email, key: UD_emailId)
//            }
//            else
//            {
//                Email = UserDefaultManager.getStringFromUserDefaults(key: UD_emailId)
//            }
//            //            print(appleIDCredential.fullName)
//            if appleIDCredential.fullName?.givenName != nil
//            {
//                Fullname = (appleIDCredential.fullName?.givenName)! + (appleIDCredential.fullName?.familyName)!
//                UserDefaultManager.setStringToUserDefaults(value: Fullname, key: UD_userFirstName)
//            }
//            else{
//                Fullname = UserDefaultManager.getStringFromUserDefaults(key: UD_userFirstName)
//            }
//            let urlString = API_URL + "login"
//            let params: NSDictionary = ["email":Email,
//                                        "password":"",
//                                        "device_type":App_device_type,
//                                        "google_id":"",
//                                        "facebook_id":"",
//                                        "apple_id":userIdentifier,
//                                        "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken)]
//            self.Webservice_Login(url: urlString, params: params)
//
//            break
//        default:
//            break
//        }
//    }
//    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//        return self.view.window!
//    }
//}
