//
//  SignupEmailViewController.swift
//  Taskly
//
//  Created by Vrushik on 07/03/22.
//

import UIKit

class SignupEmailViewController: UIViewController {
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - InitialUISetup
    
    func setupInitialUI(){
    }
    
    // MARK: - IBActions
    
    @IBAction func onClickSignup(_ sender: Any)
    {
        if txtFullname.text! == ""{
            self.view.makeToast("Fullname Field can not be empty")
        }else if txtPhone.text! == ""{
            self.view.makeToast("Phone number Field can not be empty")
        }else if txtPhone.text! != "" && !txtPhone.text!.isPhone(){
            self.view.makeToast("Please enter valid Phone number")
        }else if txtEmail.text! == ""{
            self.view.makeToast("Email Field can not be empty")
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            self.view.makeToast("Please enter valid Email Address")
        }else if txtPass.text! == ""{
            self.view.makeToast("Password Field can not be empty")
        }else if txtPass.text! != "" && !txtPass.text!.isValidPassword(){
            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
        }else{
            let param:[String:Any] = ["email":txtEmail.text!,
                                      "password":txtPass.text!,
                                      "device_type":App_device_type,
                                      "token":getFCMToken()]
            self.makeAPIcallforSignup(param)
        }
        
    }
    
    @IBAction func onClickForgotpassword(_ sender: Any)
    {
        
    }
    
    
    // MARK: - API
    
    func makeAPIcallforSignup(_ param:[String:Any])
    {
//        let headers:HTTPHeaders = ["":""]
        AIServiceManager.sharedManager.callPostApi(URL_Login, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                       let msg = data["message"] as? String
                        self.view.makeToast(msg!)
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

}
