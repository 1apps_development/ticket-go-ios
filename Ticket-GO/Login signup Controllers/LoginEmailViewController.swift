//
//  LoginEmailViewController.swift
//  Taskly
//
//  Created by Vrushik on 07/03/22.
//

import UIKit
import Alamofire

class LoginEmailViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - InitialUISetup
    
    func setupInitialUI(){
        if IS_DEMO_MODE == true {
            self.txtEmail.isUserInteractionEnabled = false
            self.txtPass.isUserInteractionEnabled = false
        }
        self.btnHideShow.setImage(UIImage.init(named: "password eye"), for: .normal)
        self.btnHideShow.setImage(UIImage(named: "password eye close"), for: .selected)
    }
    
    // MARK: - IBActions
    
    @IBAction func onClickHideShow(_ sender: UIButton )
    {
        if sender.isSelected{
            btnHideShow.isSelected = false
            txtPass.isSecureTextEntry = true
        }else{
            btnHideShow.isSelected = true
            txtPass.isSecureTextEntry = false
        }
    }
    
    @IBAction func onClickLogin(_ sender: Any)
    {
        if txtEmail.text! == ""{
            self.view.makeToast("Email Field can not be empty")
        }else if txtEmail.text! != "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!) == false{
            self.view.makeToast("Please enter valid Email Address")
        }else if txtPass.text! == ""{
            self.view.makeToast("Password Field can not be empty")
        }
//        else if txtPass.text! != "" && !txtPass.text!.isValidPassword(){
//            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
//        }
        else{
            let param:[String:Any] = ["email":txtEmail.text!,
                                      "password":txtPass.text!,
                                      "device_type":App_device_type,
                                      "token":getFCMToken()]
            self.makeAPIcallforLogin(param)
        }
    }
    
    @IBAction func onClickSignup(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
     }
    
    @IBAction func onClickForgotpassword(_ sender: Any)
    {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
            
    }
    
    
    // MARK: - API
    
    func makeAPIcallforLogin(_ param:[String:Any], bypass:Bool = false)
    {
//        let headers:HTTPHeaders = ["":""]
        if bypass{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            AIServiceManager.sharedManager.callPostApi(URL_Login, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                            }
                            if let token = data["access_token"] as? String{
                                setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
